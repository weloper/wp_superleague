<?php
/*
  Plugin Name: Superleague Custom Plugin
  Description: Superleague Custom Plugin
  Author: Superleague
  Text Domain: Superleague
  Version: 0.0.1
*/

// Register Custom Post Type
function bld_super() {

	$labels = array(
		'name'                  => _x( 'Super Athletes', 'Post Type General Name', 'super_league' ),
		'singular_name'         => _x( 'Athlete', 'Post Type Singular Name', 'super_league' ),
		'menu_name'             => __( 'Super Athletes', 'super_league' ),
		'name_admin_bar'        => __( 'Super athlete', 'super_league' ),
		'archives'              => __( 'Athlete Archives', 'super_league' ),
		'attributes'            => __( 'Athlete Attributes', 'super_league' ),
		'parent_item_colon'     => __( 'Parent Athlete:', 'super_league' ),
		'all_items'             => __( 'All Athletes', 'super_league' ),
		'add_new_item'          => __( 'Add New Athlete', 'super_league' ),
		'add_new'               => __( 'Add New', 'super_league' ),
		'new_item'              => __( 'New Athlete', 'super_league' ),
		'edit_item'             => __( 'Edit Athlete', 'super_league' ),
		'update_item'           => __( 'Update Athlete', 'super_league' ),
		'view_item'             => __( 'View Athlete', 'super_league' ),
		'view_items'            => __( 'View Athletes', 'super_league' ),
		'search_items'          => __( 'Search Athlete', 'super_league' ),
		'not_found'             => __( 'Not found', 'super_league' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'super_league' ),
		'featured_image'        => __( 'Featured Image', 'super_league' ),
		'set_featured_image'    => __( 'Set featured image', 'super_league' ),
		'remove_featured_image' => __( 'Remove featured image', 'super_league' ),
		'use_featured_image'    => __( 'Use as featured image', 'super_league' ),
		'insert_into_item'      => __( 'Insert into item', 'super_league' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'super_league' ),
		'items_list'            => __( 'Items list', 'super_league' ),
		'items_list_navigation' => __( 'Items list navigation', 'super_league' ),
		'filter_items_list'     => __( 'Filter items list', 'super_league' ),
	);
	$args = array(
		'label'                 => __( 'Super Athletes', 'super_league' ),
		'description'           => __( 'Super Athletes Description', 'super_league' ),
		'labels'                => $labels,
		'supports'              => array( 'title' ),
		'taxonomies'            => array( 'athlete_gender', 'athlete_category' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'super_athlete', $args );

}
add_action( 'init', 'bld_super', 0 );

// Register Custom Taxonomy
function athlete_gender() {

	$labels = array(
		'name'                       => _x( 'Athlete Genders', 'Taxonomy General Name', 'superleague' ),
		'singular_name'              => _x( 'Athlete Gender', 'Taxonomy Singular Name', 'superleague' ),
		'menu_name'                  => __( 'Athlete Gender', 'superleague' ),
		'all_items'                  => __( 'All Genders', 'superleague' ),
		'parent_item'                => __( 'Parent Gender', 'superleague' ),
		'parent_item_colon'          => __( 'Parent Gender:', 'superleague' ),
		'new_item_name'              => __( 'New Gender Name', 'superleague' ),
		'add_new_item'               => __( 'Add New Gender', 'superleague' ),
		'edit_item'                  => __( 'Edit Gender', 'superleague' ),
		'update_item'                => __( 'Update Gender', 'superleague' ),
		'view_item'                  => __( 'View Gender', 'superleague' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'superleague' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'superleague' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'superleague' ),
		'popular_items'              => __( 'Popular Items', 'superleague' ),
		'search_items'               => __( 'Search Items', 'superleague' ),
		'not_found'                  => __( 'Not Found', 'superleague' ),
		'no_terms'                   => __( 'No items', 'superleague' ),
		'items_list'                 => __( 'Items list', 'superleague' ),
		'items_list_navigation'      => __( 'Items list navigation', 'superleague' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
	);
	register_taxonomy( 'athlete_gender', array( 'super_athlete' ), $args );

}
add_action( 'init', 'athlete_gender', 0 );

// Register Custom Taxonomy
function athlete_category() {

	$labels = array(
		'name'                       => _x( 'Athlete Category', 'Taxonomy General Name', 'superleague' ),
		'singular_name'              => _x( 'Athlete Category', 'Taxonomy Singular Name', 'superleague' ),
		'menu_name'                  => __( 'Athlete Category', 'superleague' ),
		'all_items'                  => __( 'All Category', 'superleague' ),
		'parent_item'                => __( 'Parent Item', 'superleague' ),
		'parent_item_colon'          => __( 'Parent Item:', 'superleague' ),
		'new_item_name'              => __( 'New Item Name', 'superleague' ),
		'add_new_item'               => __( 'Add New Item', 'superleague' ),
		'edit_item'                  => __( 'Edit Item', 'superleague' ),
		'update_item'                => __( 'Update Item', 'superleague' ),
		'view_item'                  => __( 'View Item', 'superleague' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'superleague' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'superleague' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'superleague' ),
		'popular_items'              => __( 'Popular Items', 'superleague' ),
		'search_items'               => __( 'Search Items', 'superleague' ),
		'not_found'                  => __( 'Not Found', 'superleague' ),
		'no_terms'                   => __( 'No items', 'superleague' ),
		'items_list'                 => __( 'Items list', 'superleague' ),
		'items_list_navigation'      => __( 'Items list navigation', 'superleague' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
	);
	register_taxonomy( 'athlete_category', array( 'super_athlete' ), $args );

}
add_action( 'init', 'athlete_category', 0 );


add_shortcode( 'superathlete', 'super_athlete_shortcode' );
function super_athlete_shortcode( $atts ) {
    ob_start();
 
    // define attributes and their defaults
    extract( shortcode_atts( array (
        'type' => 'super_athlete',
        'order' => 'date',
        'orderby' => 'title',
        'posts' => -1,
        'athlete_gender' => '',
        'athlete_category' => '',
    ), $atts ) );
 
    // define query parameters based on attributes
    $options = array(
        'post_type' => $type,
        'order' => $order,
        'orderby' => $orderby,
        'posts_per_page' => $posts,
        'athlete_gender' => $athlete_gender,
        'athlete_category' => $athlete_category,
    );
    $query = new WP_Query( $options );
    // run the loop based on the query

    if ( $query->have_posts() ) { ?>
        <ul class="clothes-listing ">
        	<?php while ( $query->have_posts() ) : $query->the_post(); ?>
            <li id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            </li>
            <?php endwhile;
            wp_reset_postdata(); ?>
        </ul>
    <?php
        $myvariable = ob_get_clean();
        return $myvariable;
    }
}


/* Accordion Section */

function home_accordion_enqueue_script() {   
	if( is_front_page() ){
	    wp_enqueue_script( 'main_script', plugin_dir_url( __FILE__ ) . 'js/main.js', array('jquery'), '1.0' );
	    wp_enqueue_script( 'modernizr', plugin_dir_url( __FILE__ ) . 'js/modernizr.js', array('jquery'), '1.0' );    
	    wp_enqueue_style( 'style', plugins_url( '/css/style.css', __FILE__ ) );
	    wp_enqueue_style( 'reset', plugins_url( '/css/reset.css', __FILE__ ) );		
	}

}
//add_action('wp_enqueue_scripts', 'home_accordion_enqueue_script');

/* Create Shortcode for Accordion */

function super_accordion(){

	$value = '
	<ul class="cd-accordion-menu animated">
		<li class="has-children">
			<input type="checkbox" name ="group-1" id="group-1" checked>
			<label for="group-1">BLD</label>

				<ul>
					<li>
						<table class="table">
						  <thead class="thead-dark">
						    <tr>
						      <th scope="col">Rank</th>
						      <th scope="col">Name</th>
						      <th scope="col">Wightclass</th>
						      <th scope="col">Handle</th>
						    </tr>
						  </thead>
						  <tbody>
						    <tr>
						      <td>1</td>
						      <td>Mark</td>
						      <td>Otto</td>
						      <td>@mdo</td>
						    </tr>
						  </tbody>
						</table>
					</li>
				</ul>
		</li>

		<li class="has-children">
			<input type="checkbox" name ="group-2" id="group-2">
			<label for="group-2">MTR</label>

			<ul>
				<li>
					<table class="table">
					  <thead class="thead-dark">
					    <tr>
					      <th scope="col">#</th>
					      <th scope="col">First</th>
					      <th scope="col">Last</th>
					      <th scope="col">Handle</th>
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <th scope="row">1</th>
					      <td>Mark</td>
					      <td>Otto</td>
					      <td>@mdo</td>
					    </tr>
					    <tr>
					      <th scope="row">2</th>
					      <td>Jacob</td>
					      <td>Thornton</td>
					      <td>@fat</td>
					    </tr>
					    <tr>
					      <th scope="row">3</th>
					      <td>Larry</td>
					      <td>the Bird</td>
					      <td>@twitter</td>
					    </tr>
					  </tbody>
					</table>
				</li>
			</ul>
		</li>

		<li class="has-children">
			<input type="checkbox" name ="group-3" id="group-3">
			<label for="group-3">MXO</label>

			<ul>
				<li>
					<table class="table">
					  <thead class="thead-dark">
					    <tr>
					      <th scope="col">#</th>
					      <th scope="col">First</th>
					      <th scope="col">Last</th>
					      <th scope="col">Handle</th>
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <th scope="row">1</th>
					      <td>Mark</td>
					      <td>Otto</td>
					      <td>@mdo</td>
					    </tr>
					    <tr>
					      <th scope="row">2</th>
					      <td>Jacob</td>
					      <td>Thornton</td>
					      <td>@fat</td>
					    </tr>
					    <tr>
					      <th scope="row">3</th>
					      <td>Larry</td>
					      <td>the Bird</td>
					      <td>@twitter</td>
					    </tr>
					  </tbody>
					</table>
				</li>
			</ul>
		</li>

		<li class="has-children">
			<input type="checkbox" name ="group-4" id="group-4">
			<label for="group-4">OWR</label>

			<ul>
				<li>
					<table class="table">
					  <thead class="thead-dark">
					    <tr>
					      <th scope="col">#</th>
					      <th scope="col">First</th>
					      <th scope="col">Last</th>
					      <th scope="col">Handle</th>
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <th scope="row">1</th>
					      <td>Mark</td>
					      <td>Otto</td>
					      <td>@mdo</td>
					    </tr>
					    <tr>
					      <th scope="row">2</th>
					      <td>Jacob</td>
					      <td>Thornton</td>
					      <td>@fat</td>
					    </tr>
					    <tr>
					      <th scope="row">3</th>
					      <td>Larry</td>
					      <td>the Bird</td>
					      <td>@twitter</td>
					    </tr>
					  </tbody>
					</table>
				</li>
			</ul>
		</li>
	</ul>';
	
   return $value;

}

add_shortcode('super-accordion','super_accordion');



function super_table(){

	$category = get_the_title();

	$value = '
	<div class="row">
		<div class="col-md-6">
			<h4>Male</h4>
			<table class="table">
			  <thead class="thead-dark">
			    <tr>
			      <th scope="col">Rank</th>
			      <th scope="col">Name</th>
			      <th scope="col">Weight Class</th>
			      <th scope="col">Score</th>
			    </tr>
			  </thead>
			  <tbody>';

    // define attributes and their defaults
 
    // define query parameters based on attributes
    $options = array(
        'post_type' => 'super_athlete',
        'meta_key' => 'rank',
        'orderby' => 'meta_value_num',
        'order' => 'ASC',
        'posts_per_page' => 10,
        'athlete_gender' => 'men',
        'athlete_category' => $category,
    );
    $query = new WP_Query( $options );
    // run the loop based on the query

    $i=1;
    if ( $query->have_posts() ) { 
        $value .= '<ul class="clothes-listing ">';
        	while ( $query->have_posts() ) : $query->the_post();
        	$rank = get_post_meta( get_the_ID(), 'rank', true );
        	$weight = get_post_meta( get_the_ID(), 'weight', true );
        	$score = get_post_meta( get_the_ID(), 'score', true );
        	
	    	$value .= '<tr>';
		    $value .= '<td>'.$rank.'</td>';
		    $value .= '<td>'. get_the_title(). '</td>';
		    $value .= '<td>'.$weight. '</td>';
		    $value .= '<td>'.$score. '</td>';
		    $value .= '</tr>';
		    $i++;
            endwhile;
            wp_reset_postdata();
        $value .= '</ul>';
    }
    

			    $value .= '
			  </tbody>
			</table>
		</div>
		<div class="col-md-6">
			<h4>Female</h4>
			<table class="table">
			  <thead class="thead-dark">
			    <tr>
			      <th scope="col">Rank</th>
			      <th scope="col">Name</th>
			      <th scope="col">Weight Class</th>
			      <th scope="col">Score</th>
			    </tr>
			  </thead>
			  <tbody>';

    // define attributes and their defaults
 
    // define query parameters based on attributes
    $options = array(
        'post_type' => 'super_athlete',
        'meta_key' => 'rank',
        'orderby' => 'meta_value_num',
        'order' => 'ASC',
        'posts_per_page' => 10,
        'athlete_gender' => 'women',
        'athlete_category' => $category,
    );
    $query = new WP_Query( $options );
    // run the loop based on the query

    $i=1;
    if ( $query->have_posts() ) { 
        $value .= '<ul class="clothes-listing ">';
        	while ( $query->have_posts() ) : $query->the_post();
        	$rank = get_post_meta( get_the_ID(), 'rank', true );
        	$weight = get_post_meta( get_the_ID(), 'weight', true );
        	$score = get_post_meta( get_the_ID(), 'score', true );
        	
	    	$value .= '<tr>';
		    $value .= '<td>'.$rank.'</td>';
		    $value .= '<td>'. get_the_title(). '</td>';
		    $value .= '<td>'.$weight. '</td>';
		    $value .= '<td>'.$score. '</td>';
		    $value .= '</tr>';
		    $i++;
            endwhile;
            wp_reset_postdata();
        $value .= '</ul>';
    }
    

			    $value .= '
			  </tbody>
			</table>
		</div>
	</div>';
	
   return $value;

}

add_shortcode('super-table','super_table');



function super_owr_table(){

	$category = get_the_title();

	$value = '
	<div class="row">
		<div class="col-md-6">
			<h4>Male</h4>
			<table class="table">
			  <thead class="thead-dark">
			    <tr>
			      <th scope="col">Movements</th>
			      <th scope="col">Rank</th>
			      <th scope="col">Name</th>
			      <th scope="col">Score</th>
			    </tr>
			  </thead>
			  <tbody>';

    // define attributes and their defaults
 
    // define query parameters based on attributes
    $options = array(
        'post_type' => 'super_athlete',
        'meta_key' => 'rank',
        'orderby' => 'meta_value_num',
        'order' => 'ASC',
        'posts_per_page' => 10,
        'athlete_gender' => 'men',
        'athlete_category' => $category,
    );
    $query = new WP_Query( $options );
    // run the loop based on the query

    $i=1;
    if ( $query->have_posts() ) { 
        	while ( $query->have_posts() ) : $query->the_post();
        	$rank = get_post_meta( get_the_ID(), 'rank', true );
        	$weight = get_post_meta( get_the_ID(), 'weight', true );
        	$movements = get_post_meta( get_the_ID(), 'movements', true );
        	$score = get_post_meta( get_the_ID(), 'score', true );
        	
	    	$value .= '<tr>';
		    $value .= '<td>'.$movements. '</td>';
		    $value .= '<td>'.$rank.'</td>';
		    $value .= '<td>'. get_the_title(). '</td>';
		    $value .= '<td>'.$score. '</td>';
		    $value .= '</tr>';
		    $i++;
            endwhile;
            wp_reset_postdata();
    }
    

			    $value .= '
			  </tbody>
			</table>
		</div>
		<div class="col-md-6">
			<h4>Female</h4>
			<table class="table">
			  <thead class="thead-dark">
			    <tr>
			      <th scope="col">Movements</th>
			      <th scope="col">Rank</th>
			      <th scope="col">Name</th>
			      <th scope="col">Score</th>
			    </tr>
			  </thead>
			  <tbody>';

    // define attributes and their defaults
 
    // define query parameters based on attributes
    $options = array(
        'post_type' => 'super_athlete',
        'meta_key' => 'rank',
        'orderby' => 'meta_value_num',
        'order' => 'ASC',
        'posts_per_page' => 10,
        'athlete_gender' => 'women',
        'athlete_category' => $category,
    );
    $query = new WP_Query( $options );
    // run the loop based on the query

    $i=1;
    if ( $query->have_posts() ) { 
        	while ( $query->have_posts() ) : $query->the_post();
        	$rank = get_post_meta( get_the_ID(), 'rank', true );
        	$movements = get_post_meta( get_the_ID(), 'movements', true );
        	$weight = get_post_meta( get_the_ID(), 'weight', true );
        	$score = get_post_meta( get_the_ID(), 'score', true );
        	
	    	$value .= '<tr>';
		    $value .= '<td>'.$movements. '</td>';
		    $value .= '<td>'.$rank.'</td>';
		    $value .= '<td>'. get_the_title(). '</td>';
		    $value .= '<td>'.$score. '</td>';
		    $value .= '</tr>';
		    $i++;
            endwhile;
            wp_reset_postdata();
    }
    

			    $value .= '
			  </tbody>
			</table>
		</div>
	</div>';
	
   return $value;

}

add_shortcode('super-owr-table','super_owr_table');




/* Champions List */


// Register Custom Post Type
function super_champions() {

	$labels = array(
		'name'                  => _x( 'Super Champions', 'Post Type General Name', 'super_league' ),
		'singular_name'         => _x( 'Champion', 'Post Type Singular Name', 'super_league' ),
		'menu_name'             => __( 'Super Champions', 'super_league' ),
		'name_admin_bar'        => __( 'Super Champion', 'super_league' ),
		'archives'              => __( 'Champion Archives', 'super_league' ),
		'attributes'            => __( 'Champion Attributes', 'super_league' ),
		'parent_item_colon'     => __( 'Parent Champion:', 'super_league' ),
		'all_items'             => __( 'All Champions', 'super_league' ),
		'add_new_item'          => __( 'Add New Champion', 'super_league' ),
		'add_new'               => __( 'Add New', 'super_league' ),
		'new_item'              => __( 'New Champion', 'super_league' ),
		'edit_item'             => __( 'Edit Champion', 'super_league' ),
		'update_item'           => __( 'Update Champion', 'super_league' ),
		'view_item'             => __( 'View Champion', 'super_league' ),
		'view_items'            => __( 'View Champions', 'super_league' ),
		'search_items'          => __( 'Search Champion', 'super_league' ),
		'not_found'             => __( 'Not found', 'super_league' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'super_league' ),
		'featured_image'        => __( 'Featured Image', 'super_league' ),
		'set_featured_image'    => __( 'Set featured image', 'super_league' ),
		'remove_featured_image' => __( 'Remove featured image', 'super_league' ),
		'use_featured_image'    => __( 'Use as featured image', 'super_league' ),
		'insert_into_item'      => __( 'Insert into item', 'super_league' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'super_league' ),
		'items_list'            => __( 'Items list', 'super_league' ),
		'items_list_navigation' => __( 'Items list navigation', 'super_league' ),
		'filter_items_list'     => __( 'Filter items list', 'super_league' ),
	);
	$args = array(
		'label'                 => __( 'Super Champions', 'super_league' ),
		'description'           => __( 'Super Champions Description', 'super_league' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'            => array( '' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'super_champions', $args );

}
add_action( 'init', 'super_champions', 0 );



add_shortcode( 'super-champions', 'super_champions_shortcode' );
function super_champions_shortcode( $atts ) {
    ob_start();
 
    // define attributes and their defaults
    extract( shortcode_atts( array (
        'type' => 'super_champions',
        'order' => 'date',
        'orderby' => 'title',
        'posts' => 8,
    ), $atts ) );
 
    // define query parameters based on attributes
    $options = array(
        'post_type' => $type,
        'order' => $order,
        'orderby' => $orderby,
        'posts_per_page' => $posts
    );
    $query = new WP_Query( $options );
    // run the loop based on the query

    if ( $query->have_posts() ) { ?>
    	<div class="row">
        	<?php while ( $query->have_posts() ) : $query->the_post(); ?>
    		<?php $url = wp_get_attachment_url( get_post_thumbnail_id($query->ID), 'thumbnail' ); ?>
			<div class="col-xs-6 col-md-3 cham_css" id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
            	<img src="<?php echo $url ?>" />
                <div class="cham_content"><?php the_content(); ?></div>
                <div class="cham_title"><?php the_title(); ?></div>
            </div>
            <?php endwhile;
            wp_reset_postdata(); ?>
        </div>
    <?php
        $super_champions = ob_get_clean();
        return $super_champions;
    }
}

/* Custom Fields for registration form */
function mytheme_add_fields_to_signup(){
	//don't break if Register Helper is not loaded
	if(!function_exists( 'pmprorh_add_registration_field' )) {
		return false;
	}
	
	$fields = array();

	$fields[] = new PMProRH_Field(
		'first_name',// input name, will also be used as meta key
		'text',	// type of field
		array(
			'label'		=> 'First Name',
			'size'		=> 40,
			'profile'	=> true,
			'required'	=> true, 
		)
	);

	$fields[] = new PMProRH_Field(
		'last_name',
		'text',
		array(
			'label'		=> 'Last Name',
			'size'		=> 40,
			'profile'	=> true,
			'required'	=> true, 
		)
	);

	$fields[] = new PMProRH_Field(
		'instagram',
		'text',
		array(
			'label'		=> 'Instagram Handle',
			'size'		=> 40,
			'profile'	=> true,
			'required'	=> true, 
		)
	);

	//add the fields to default forms
	foreach($fields as $field){
		pmprorh_add_registration_field(
			'after_username',	
			$field	
		);
	}

	$extra_fields = array();


	$extra_fields[] = new PMProRH_Field(
		'birthdate',
		'text',
		array(
			'label'		=> 'Birth date',
			'size'		=> 40,
			'profile'	=> true,
			'required'	=> true, 
		)
	);

	$extra_fields[] = new PMProRH_Field(
		"gender", 
		"select", 
		array(
			"options"=>array(
				"male"=>"Male", 
				"female"=>"Female"
			)
		)
	);

	$extra_fields[] = new PMProRH_Field(
		'shirt_size',
		'text',
		array(
			'label'		=> 'Shirt Size',
			'size'		=> 40,
			'profile'	=> true,
			'required'	=> true, 
		)
	);

	//add the fields to default forms
	foreach($extra_fields as $extra_field){
		pmprorh_add_registration_field(
			'after_email',	
			$extra_field	
		);
	}

}
add_action( 'init', 'mytheme_add_fields_to_signup' );

function hide_footer(){
	if ( is_front_page() ) {
	echo '
		<style>
			footer.footer.footer-black.footer-big .content {
			    /*display: none;*/
			}
		</style>';
	}

}

add_action('wp_footer','hide_footer');

function youtube_video(){
	echo '<style>.carousel-inner .item {
    display: none;
}
.norm_row.sfsi_wDiv {
    margin: 0px auto !important;
    text-align: center !important;
    width: 100% !important;
}
.social_cashs {
    background: #000;
    margin-top: -6px;
    padding-top: 5px;
    text-align: center;
}</style>';
echo '<div class="youtube_video">
	<iframe src="https://www.youtube.com/embed/P_Upxe-ORnc?controls=0&amp;showinfo=0&amp;modestbranding=1&amp;controls=0" width="100%" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
	</div>'
	;
	echo '<div class="social_cashs">';
	echo do_shortcode('[DISPLAY_ULTIMATE_SOCIAL_ICONS]');
	echo '</div>';

}
add_action('hestia_first_front_page_section_content','youtube_video');


function hide_title(){

	if( is_page( array( 'bld', 'mtr', 'mxo', 'owr', 'membership-plans', 'membership-checkout', 'membership-billing', 'membership-confirmation' ) ) ) {
		echo '<style>
			.page-template h1.hestia-title {
			    display: none;
			}
			.page-template-default h1.hestia-title {
			    display: none;
			}
			</style>';
	}
	if( is_page( array('join-the-league', 'new-membership-plans', 'contact-us') ) ) {
		echo '<style>
		h1.hestia-title {
		    display: none;
		}
		</style>';
	}

	if( is_page( 'membership-checkout' ) ) {
		echo '<style>
		.header-filter.header-filter-gradient {
    display: none;
}

.header-filter.header-filter-gradient {
    background: #000;
}
.header-filter {
    background: #000;
}
			.page-header.header-small {
				min-height: 45px;
				position: relative;
				height: auto;
			}
				.boxed-layout-header {
					padding-bottom: 0;
				}
				.page-header.header-small .container {
    padding-top: 120px !important;
    padding-bottom: 0px !important;
}
			</style>';
	}
	
}

add_action('wp_footer','hide_title');

add_action('admin_print_scripts-profile.php', 'hide_admin_bar_prefs');
function hide_admin_bar_prefs() { ?>
<style type="text/css">
	.show-admin-bar {display: none;}
</style>
<?php
}
add_filter('show_admin_bar', '__return_false');
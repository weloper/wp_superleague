<?php 
global $wpdb, $pmpro_msg, $pmpro_msgt, $current_user;

$pmpro_levels = pmpro_getAllLevels(false, true);
$pmpro_level_order = pmpro_getOption('level_order');

if(!empty($pmpro_level_order))
{
	$order = explode(',',$pmpro_level_order);

	//reorder array
	$reordered_levels = array();
	foreach($order as $level_id) {
		foreach($pmpro_levels as $key=>$level) {
			if($level_id == $level->id)
				$reordered_levels[] = $pmpro_levels[$key];
		}
	}

	$pmpro_levels = $reordered_levels;
}

$pmpro_levels = apply_filters("pmpro_levels_array", $pmpro_levels);

if($pmpro_msg)
{
?>
<div class="pmpro_message <?php echo $pmpro_msgt?>"><?php echo $pmpro_msg?></div>
<?php
}
?>
<!-- <table id="pmpro_levels_table" class="pmpro_checkout">
<thead>
  <tr>
	<th><?php _e('Level', 'paid-memberships-pro' );?></th>
	<th><?php _e('Price', 'paid-memberships-pro' );?></th>	
	<th>&nbsp;</th>
  </tr>
</thead>
<tbody>
	<?php	
	$count = 0;
	foreach($pmpro_levels as $level)
	{
	  if(isset($current_user->membership_level->ID))
		  $current_level = ($current_user->membership_level->ID == $level->id);
	  else
		  $current_level = false;
	?>
	<tr class="<?php if($count++ % 2 == 0) { ?>odd<?php } ?><?php if($current_level == $level) { ?> active<?php } ?>">
		<td><?php echo $current_level ? "<strong>{$level->name}</strong>" : $level->name?></td>
		<td>
			<?php 
				if(pmpro_isLevelFree($level))
					$cost_text = "<strong>" . __("Free", 'paid-memberships-pro' ) . "</strong>";
				else
					$cost_text = pmpro_getLevelCost($level, true, true); 
				$expiration_text = pmpro_getLevelExpiration($level);
				if(!empty($cost_text) && !empty($expiration_text))
					echo $cost_text . "<br />" . $expiration_text;
				elseif(!empty($cost_text))
					echo $cost_text;
				elseif(!empty($expiration_text))
					echo $expiration_text;
			?>
		</td>
		<td>
		<?php if(empty($current_user->membership_level->ID)) { ?>
			<a class="pmpro_btn pmpro_btn-select" href="<?php echo pmpro_url("checkout", "?level=" . $level->id, "https")?>"><?php _e('Select', 'paid-memberships-pro' );?></a>
		<?php } elseif ( !$current_level ) { ?>                	
			<a class="pmpro_btn pmpro_btn-select" href="<?php echo pmpro_url("checkout", "?level=" . $level->id, "https")?>"><?php _e('Select', 'paid-memberships-pro' );?></a>
		<?php } elseif($current_level) { ?>      
			
			<?php
				//if it's a one-time-payment level, offer a link to renew				
				if( pmpro_isLevelExpiringSoon( $current_user->membership_level) && $current_user->membership_level->allow_signups ) {
					?>
						<a class="pmpro_btn pmpro_btn-select" href="<?php echo pmpro_url("checkout", "?level=" . $level->id, "https")?>"><?php _e('Renew', 'paid-memberships-pro' );?></a>
					<?php
				} else {
					?>
						<a class="pmpro_btn disabled" href="<?php echo pmpro_url("account")?>"><?php _e('Your&nbsp;Level', 'paid-memberships-pro' );?></a>
					<?php
				}
			?>
			
		<?php } ?>
		</td>
	</tr>
	<?php
	}
	?>
</tbody>
</table> -->
<div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="pricingTable">
                    <div class="pricingTable-header">
                        <h3>Free (Level 1)</h3>
                    </div>
                    <div class="pricing-plans">
                        <span class="price-value"><i class="fa fa-usd"></i><span>0.00</span></span>
                        <span class="month">/month</span>
                    </div>
                    <div class="pricingContent">
                        <ul>
                            <li>First score Free</li>
                            <li>No Live streaming Via our Twitch account available</li>
                            <li>Access to the SL app</li>
                            <li>Customized Profile</li>
                            <li>No apparel</li>
                            <li>Non Priority Unlimited email communication with the Athlete Director</li>
                            <li>No Revenue opputunities</li>
                            <li>No Rewards program</li>
                            <li>No Access to the Las Vegas Arena</li>
                        </ul>
                    </div><!-- CONTENT BOX-->
                    <div class="pricingTable-sign-up">
                        <a href="<?php echo site_url(); ?>/membership-account/membership-checkout/?level=1" class="btn btn-block">sign up</a>
                    </div><!-- BUTTON BOX-->
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="pricingTable">
                    <div class="pricingTable-header">
                        <h3>Level 2</h3>
                    </div>
                    <div class="pricing-plans">
                        <span class="price-value"><i class="fa fa-usd"></i><span>19.00</span></span>
                        <span class="month">/month</span>
                    </div>
                    <div class="pricingContent">
                        <ul>
                            <li>Post one score per month</li>
                            <li>No Live streaming Via our Twitch account available</li>
                            <li>Access to the SL app</li>
                            <li>Customized Profile</li>
                            <li>T-shirt/tank when you join</li>
                            <li>Unlimited email Communication with the Athlete Director</li>
                            <li>Referral opportunity</li>
                            <li>No rewards Program</li>
                            <li>No Access to the Las Vegas Arena</li>
                        </ul>
                    </div><!-- CONTENT BOX-->
                    <div class="pricingTable-sign-up">
                        <a href="<?php echo site_url(); ?>/membership-account/membership-checkout/?level=2" class="btn btn-block">sign up</a>
                    </div><!-- BUTTON BOX-->
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="pricingTable">
                    <div class="pricingTable-header">
                        <h3>Level 3</h3>
                    </div>
                    <div class="pricing-plans">
                        <span class="price-value"><i class="fa fa-usd"></i><span>29.00</span></span>
                        <span class="month">/month</span>
                    </div>
                    <div class="pricingContent">
                        <ul>
                            <li>Post unlimited scores per month</li>
                            <li>Live streaming via our Twitch account</li>
                            <li>Access to the SL App</li>
                            <li>Customize Profile</li>
                            <li>T-shirt/tank when you join</li>
                            <li>Unlimited email communication with the Athlete Director</li>
                            <li>Team Opptunity</li>
                            <li>Kilo rewards program - earn per score</li>
                            <li>Access to the Las Vagas Arena</li>
                        </ul>
                    </div><!-- CONTENT BOX-->
                    <div class="pricingTable-sign-up">
                        <a href="<?php echo site_url(); ?>/membership-account/membership-checkout/?level=3" class="btn btn-block">sign up</a>
                    </div><!-- BUTTON BOX-->
                </div>
            </div>
        </div>

        <p><br></p>
<nav id="nav-below" class="navigation" role="navigation">
	<div class="nav-previous alignleft">
		<?php if(!empty($current_user->membership_level->ID)) { ?>
			<a href="<?php echo pmpro_url("account")?>" id="pmpro_levels-return-account"><?php _e('&larr; Return to Your Account', 'paid-memberships-pro' );?></a>
		<?php } else { ?>
			<a href="<?php echo home_url()?>" id="pmpro_levels-return-home"><?php _e('&larr; Return to Home', 'paid-memberships-pro' );?></a>
		<?php } ?>
	</div>
</nav>

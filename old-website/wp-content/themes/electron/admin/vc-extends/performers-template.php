<?php
/**
 * The VC Functions
 */
function electron_performer_carousel_settings_vc() {
    vc_map(
    array(
      'name'       => __( 'Performers', 'electron' ),
        'base' => 'perch_performers',
        'category'     => 'Electron',
        'params' => array(
           array(
                'type' => 'textfield',
                'value' => 'EVENT PERFORMERS',
                'heading' => 'Title',
                'param_name' => 'title',
                'admin_label' => true,
                'description' => 'Leave blank to avaoid title'
            ),
            array(
                'type' => 'perch_select',
                'value' => array(
                        'templates/performer-carousel.php' => 'Carousel', 
                        'templates/performer-grid.php' => 'Grid'
                    ),
                'heading' => 'Performer display',
                'param_name' => 'template',
            ),
            array(
                'type' => 'perch_select',
                'value' => array('all' => 'All', 'specific' => 'Specific performer'),
                'heading' => 'Performer display',
                'param_name' => 'display',
            ),
            // params group
            array(
                'type' => 'param_group',
                'value' => '',
                'heading' => __( 'Performers', 'electron' ),
                'param_name' => 'performers',
                'value' => '',
                'params' => array(
                    array(
                        'type' => 'perch_select',
                        'value' => electron_get_posts_dropdown(array('post_type' => 'performer', 'posts_per_page' => -1)),
                        'heading' => 'Performer',
                        'param_name' => 'performer',
                        'admin_label' => true,
                    ),
                   
                ),
                'dependency' => array(
                    'element' => 'display',
                    'value' => 'specific'
                )
            ),
            array(
                'type' => 'number',
                'value' => '4',
                'heading' => 'Column',
                'param_name' => 'column',
                'min' => 1,
                'max' => 20,
                'step' => 1,
            ),
            array(
                'type' => 'perch_select',
                'value' => array('no' => 'No', 'yes' => 'Yes'),
                'heading' => 'Autoplay',
                'param_name' => 'autoplay',
                'dependency' => array(
                    'element' => 'template',
                    'value' => 'templates/performer-carousel.php'
                )
            ),
            array(
                'type' => 'perch_select',
                'value' => array('yes' => 'Yes', 'no' => 'No', ),
                'heading' => 'Next/Previous Control display',
                'param_name' => 'control',
                'dependency' => array(
                    'element' => 'template',
                    'value' => 'templates/performer-carousel.php'
                )
            )

        ),
           
    )
    );
}
add_action( 'vc_before_init', 'electron_performer_carousel_settings_vc');
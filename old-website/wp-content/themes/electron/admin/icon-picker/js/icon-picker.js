/* Icon Picker */

(function($) {

	$.fn.iconPicker = function( options ) {

		var options = ['fa','fa']; // default font set
		var icons;
		$list = $('');
		function electron_font_set() {
			if (options[0] == 'fa') {
			icons = iconset.fa;
			options[1] = "fa";
			}  else {
				icons = iconset.genericon;
				options[1] = 'genericon';
			};
	};
	electron_font_set();

	function electron_build_list($popup,$button,clear) {
	  $list = $popup.find('.icon-picker-list');
	  if (clear==1) { $list.empty(); // clear list //
	  }
	  for (var i in icons) {
		  $list.append('<li data-icon="'+icons[i]+'"><a href="#" title="'+icons[i]+'"><span class="'+options[0]+' '+options[1]+'-'+icons[i]+'"></span></a></li>');
	  };
		$('a', $list).click(function(e) {
			e.preventDefault();
			var title = $(this).attr("title");
			$target.val(options[0]+"|"+options[1]+"-"+title);
			$button.removeClass().addClass("button icon-picker "+options[0]+" "+options[1]+"-"+title);
			$target.trigger('change');
			electron_removePopup();
		});
	};
	
	function electron_removePopup(){
		$(".icon-picker-container").remove();
	}
	

	$button = $('.electron-icon-picker');
	$button.each( function() {
		$(this).on('click.iconPicker', function() {
			electron_createPopup($(this));
		});
	});


	function electron_createPopup($button) {
		$target = $($button.data('target'));
		$popup = $('<div class="icon-picker-container"> \
				<div class="icon-picker-control" /> \
				<ul class="icon-picker-list" /> \
			</div>')
			.css({
				'top': $button.offset().top,
				'left': $button.offset().left
			});
		electron_build_list($popup,$button,0);
		var $control = $popup.find('.icon-picker-control');
		$control.html('<p>Select Font: <select><option value="fa">Font Awesome</option></select></p>'+
		'<a data-direction="back" href="#"><span class="dashicons dashicons-arrow-left-alt2"></span></a> '+
		'<input type="text" class="" placeholder="Search" />'+
		'<a data-direction="forward" href="#"><span class="dashicons dashicons-arrow-right-alt2"></span></a>'+
		'');



		$('select', $control).on('change', function(e) {
			e.preventDefault();
			if (this.value != options[0]) {
				options[0] = this.value;
				electron_font_set();
				electron_build_list($popup,$button,1);
			};
		});



		$('a', $control).click(function(e) {
			electron_build_list($popup,$button,0);

			if ($(this).data('direction') === 'back') {				
				//move last 25 elements to front
				$('li:gt(' + (icons.length - 26) + ')', $list).each(function() {
					$(this).prependTo($list);
				});
			} else {
				//move first 25 elements to the end
				$('li:lt(25)', $list).each(function() {
					$(this).appendTo($list);
				});
			}
			return false;
		});

		$popup.appendTo('body').show();

		$('input', $control).on('keyup', function(e) {
			electron_build_list($popup,$button,0);
			var search = $(this).val();
			if (search === '') {
				//show all again
				$('li:lt(25)', $list).show();
			} else {
				$('li', $list).each(function() {
					if ($(this).data('icon').toString().toLowerCase().indexOf(search.toLowerCase()) !== -1) {
						$(this).show();
					} else {
						$(this).hide();
					}
				});
			}
		});



		$(document).mouseup(function (e){
			if (!$popup.is(e.target) && $popup.has(e.target).length === 0) {
				electron_removePopup();
			}
		});
	}
	}


	$(function() {
		$('.electron-icon-picker').iconPicker();
	});

}(jQuery));
jQuery(document).ready(function($){
	$('.electron-icon-picker').live('click', function(){
		$('.electron-icon-picker').iconPicker();
	})
})

<?php
function electron_styling_options( $options = array() ){
	$options = array(
		array(
        'id'          => 'preset_color_1',
        'label'       => __( 'Preset color 1', 'electron' ),
        'desc'        => '',
        'std'         => '#bd2871',
        'type'        => 'colorpicker',
        'section'     => 'styling_options',        
      ),
      array(
        'id'          => 'preset_color_2',
        'label'       => __( 'Preset color 2', 'electron' ),
        'desc'        => '',
        'std'         => '#ff9000',
        'type'        => 'colorpicker',
        'section'     => 'styling_options',        
      ),
      array(
        'id'          => 'gray_color',
        'label'       => __( 'Global Gray Color', 'electron' ),
        'desc'        => '',
        'std'         => '#f8f8f8',
        'type'        => 'colorpicker',
        'section'     => 'styling_options',        
      ),
      array(
        'id'          => 'dark_color',
        'label'       => __( 'Global Dark Color', 'electron' ),
        'desc'        => '',
        'std'         => '#273034',
        'type'        => 'colorpicker',
        'section'     => 'styling_options',        
      ),
      array(
        'id'          => 'dark_acent_color',
        'label'       => __( 'Global Dark Acent Color', 'electron' ),
        'desc'        => '',
        'std'         => '#232323',
        'type'        => 'colorpicker',
        'section'     => 'styling_options',        
      ),
      array(
        'id'          => 'preset_color_css',
        'label'       => __( 'CSS', 'electron' ),
        'class'      => 'hide-field',
        'desc'        => '',
        'std'         => '
.post-wrap ul li:before, .tab-content ul li:before,
.price span, h1 span, i.prime, ul.list-default li:before, .offer h1, .tab-content h3 i, h1 span, h2 span, #upcoming .item .info i, 
.standard-menu.visible .navbar-default .navbar-nav > li > a i, .standard-menu.visible .navbar-default .navbar-nav > li > a span, .standard-menu.visible .navbar-nav li .sub-nav li a i {
    color: {{preset_color_1}};
} 
p.entry-footer a,
.prime .dblBorder:hover, .prime .dblBorder:hover .dblBorderChild {
    border-color: {{preset_color_1}};
}
#slides h3, .btn-primary, .BGprime, h3 span, .option li:hover, .btn-primary, .btn-dark:hover, a.top, .sqaureIconPrime, .owl-theme .owl-controls .owl-buttons div, #upcoming .item:hover, .standard-menu.BGlight .navbar-default .navbar-nav > li:hover, .navbar-nav li .sub-nav li:hover > a, .eventData .btn:hover, #contact_form input[type="submit"]:hover, .subscribe input[type=submit] {
    background-color: {{preset_color_1}};
} 
/* ======================= */
/* ======= SECONDARY ======= */
/* ======================= */
#celebs .item i, .main-menu li:hover > a i, .main-menu li:hover > a span, #venue strong, #venue label {
    color: {{preset_color_2}};
}
.secondary .dblBorder:hover, .secondary .dblBorder:hover .dblBorderChild {
    border-color: {{preset_color_2}};
}
#slides .slide .slide-caption .container span, .pricing .package .inner, .pricing hr, #testimonial-slider span, #subscribeForm input[type="text"] {
    border-color: {{preset_color_2}};
}
/* == Background SECONDARY == */
.navbar-default, .BGsecondary, .option li, .btn-default, #testimonial-slider .carousel-indicators li.active, .galleryImg > a span, .sqaureIconSec, .owl-theme .owl-controls .owl-page span, .tabs-left li.active, .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus, .nav-tabs > li > a:hover, .nav-tabs > li > a:focus, .pricing .package:hover, #upcoming .item .img, #testimonial-slider .carousel-indicators li, #slides, #venue .btn:hover, .featureBox:hover .sqaureIconPrime {
    background-color: {{preset_color_2}};
}
/* == Background LIGHT == */
.BGlight, .services, #upcoming .item, .standard-menu.BGlight.visible {
    background-color: {{gray_color}};
}
/* ======================= */
/* =====  DARK GREY  ===== */
/* ======================= */
h1, h2, h3, h4, h5, h6, h6 a, .phone small, .offer h1 span, .standard-menu.BGlight.visible .navbar-default .navbar-nav > li > a, .features p {
    color: {{dark_color}};
}
/* == Background Color4 == */
#slides .slide .slide-caption .container h3 span, .BGdark, .phone:hover .BGsecondary, .btn:hover, .middleBox, .btn-dark, .galleryImg, .social ul li a i, ul.tabs-left, #venue h2, .experts .highlightBox, #slides .slide .img {
    background-color: {{dark_color}};
}
nav.nav, .standard-menu.BGdark, .eventData .dblBorder:hover .dblBorderChild, .off-canvas-menu.BGdark {
    background-color: {{dark_acent_color}} !important;
}

        ',
        'type'        => 'css',
        'section'     => 'styling_options',
      ),
    );

	return apply_filters( 'electron_styling_options', $options );
}  
?>
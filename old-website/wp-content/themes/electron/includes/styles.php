<?php
function electron_hex2rgb( $color, $opacity='1' ) {
  $color = trim( $color, '#' );

  if ( strlen( $color ) == 3 ) {
    $r = hexdec( substr( $color, 0, 1 ).substr( $color, 0, 1 ) );
    $g = hexdec( substr( $color, 1, 1 ).substr( $color, 1, 1 ) );
    $b = hexdec( substr( $color, 2, 1 ).substr( $color, 2, 1 ) );
  } else if ( strlen( $color ) == 6 ) {
    $r = hexdec( substr( $color, 0, 2 ) );
    $g = hexdec( substr( $color, 2, 2 ) );
    $b = hexdec( substr( $color, 4, 2 ) );
  } else {
    return '';
  }

  return "rgba( {$r}, {$g}, {$b}, {$opacity} )";
}
/**
 * Returns CSS for the color schemes.
 *
 * @param array $colors Color scheme colors.
 * @return string Color scheme CSS.
 */
function electron_get_color_scheme_css() {
	
	/*color options*/
	$preset_color_1 = ot_get_option('preset_color_1', '#f88988');
	$preset_color_2 = ot_get_option('preset_color_2', '#ff9000');

	$css = '
#subscribeForm input[type="email"] {
	border-color: '.electron_hex2rgb($preset_color_1, .8).';
}
/* == use rgba values here == */
.contactForm .form-control:focus, .sqaureIconPrime, .subscribe input[type=submit] {
	-webkit-box-shadow: 0 0 0 0.26em '.electron_hex2rgb($preset_color_1, .5).';
	-moz-box-shadow: 0 0 0 0.26em '.electron_hex2rgb($preset_color_1, .5).';
	box-shadow: 0 0 0 0.26em '.electron_hex2rgb($preset_color_1, .5).';
}
.BGprime.opaque {
	background-color: '.electron_hex2rgb($preset_color_1, .8).';
}

/* == use rgba values here == */
.BGsblue.opaqueBG, .member .info {
	background-color: '.electron_hex2rgb($preset_color_2, .8).';
}
.highlight-overlay.light-overlay:after,
.features .boxBg {
	background-color: '.electron_hex2rgb($preset_color_2, .3).';
}
.sqaureIconSec, .featureBox:hover .sqaureIconPrime {
	-webkit-box-shadow: 0 0 0 0.26em '.electron_hex2rgb($preset_color_2, .5).';
	-moz-box-shadow: 0 0 0 0.26em '.electron_hex2rgb($preset_color_2, .5).';
	box-shadow: 0 0 0 0.26em '.electron_hex2rgb($preset_color_2, .5).';
}
@media screen and (max-width: 767px) {
	.standard-menu .navbar{ background-color: '.$preset_color_2.'; }
}
';


	return $css;
}

function electron_dynamic_style_load_to_header(){	
	echo '<style type="text/css" id="tmpl-electron-color-scheme">';
		 echo electron_get_color_scheme_css(); 
	echo '</style>';	
}
add_action( 'wp_head', 'electron_dynamic_style_load_to_header' );
=== PayPal Events ===
Contributors: scottpaterson,wp-plugin
Donate link: https://wpplugin.org/donate/
Tags: PayPal, Events, Tickets, PayPal Buttons, ecommerce
Author URI: https://wpplugin.org
Requires at least: 3.5
Tested up to: 4.8
Stable tag: 1.0.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Sell tickets for your event with PayPal. No Coding Required. Official PayPal Partner.

== Description ==
= Overview =

This plugin will allow you to sell etickets for your event on your website.

Simply install the plugin, enter your PayPal business information on the settings page, make a new event, then place the shortcode on your site.

When a customer purchases a ticket, an eticket will be emailed to them containing a QR code. Use any phone QR reader app to scan the eticket and check the customer into the event.

If you have any problems, questions, or issues please create a support request and we will get back to you quickly.

This PayPal plugin works with any WordPress theme.

WP Plugin is an offical PayPal Partner based in Boulder, Colorado. You can visit WP Plugins website at [wpplugin.org](https://wpplugin.org). Various trademarks held by their respective owners.



= PayPal Events Features =

*	View tickets sold in your WordPress admin
*	Customers receive an email eTicket that contains a QR code
*   Page / Post Button shortcode Inserter
*	Built in support 25 currencies
*	Built in support 20 languages
*	Each button can have its own language and currency
*	Each button can have its own button image
*	PayPal testing with Sandbox mode
*	Choose  from 7 different PayPal Buy Now buttons
*	Choose how the PayPal window opens
*	Add Up To 3 Items Per Event


> #### PayPal Events Pro
> We offer a Pro version of this plugin for business owners who need more features.<br />
>
> * Add Up To 15 Items Per Event
> * Sale Reduces Tickets Available
> * PayPal Account For Each Event
> * Return URL For Each Event
> * Limit Quantity Available
> * Clone Events
> * Charge Tax
> * Use A Custom Button Image
> * Further Plugin Development
>
>
> [You can learn more about the Pro version here](https://wpplugin.org/downloads/easy-paypal-events-pro/)



WP Plugin is an offical PayPal Partner. Various trademarks held by their respective owners.


== Installation ==

= Automatic Installation =
> 1. Sign in to your WordPress site as an administrator.
> 2. In the main menu go to Plugins -> Add New.
> 3. Search for Easy PayPal Events and click install.
> 4. Configure values on the settings page.
> 5. Make a new event on the events page. Place the shortcode it creates anywhere on your site to show the button.

== Frequently Asked Questions ==

= How do I use this plugin =
Configure values on the settings page. Make a new event on the events page. Place the shortcode it creates anywhere on your site to show the button.

= Can I put more then one shortcode on the same post / page? =
Yes, there is no limit to the amount you can put on one post / page, or your entire site.

== Screenshots ==
1. Events Button
2. Settings Page
3. Settings Page
4. Events Page
5. Button Inserter

== Changelog ==

= 1.0.3 =
* 7/17/2017
* Fix - Problem with form border showing with certain themes.

= 1.0.2 =
* 8/21/16
* Update - Update - Updated tested up to tag.
* Update - Update - UUpdated plugin name, dropped the word easy at the beginning.

= 1.0.1 =
* 3/8/16
* Update - Updated tested up to tag.
* Update - Updated pro url links.

= 1.0 =
* 2/16/16
* Initial release

== Upgrade Notice ==

= 1.0.3 =
* 7/17/2017
* Fix - Problem with form border showing with certain themes.

= 1.0.2 =
* 8/21/16
* Update - Update - Updated tested up to tag.
* Update - Update - UUpdated plugin name, dropped the word easy at the beginning.

= 1.0.1 =
* 3/8/16
* Update - Updated tested up to tag.
* Update - Updated pro url links.

= 1.0 =
* 2/16/16
Initial release
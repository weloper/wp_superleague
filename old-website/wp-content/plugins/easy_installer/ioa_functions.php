<?php

if(!is_admin()) return;




/**
 * Add CSS & JS Files
 */

add_action('admin_enqueue_scripts','addEASYJSFScripts');

function addEASYJSFScripts() {
			if(  isset($_GET['page']) && $_GET['page'] == 'easint'  )
			{
				wp_enqueue_style('easy-instf-css',EASY_F_PLUGIN_URL.'sprites/custom.css');
			}
		
	}
		

function addMetaDemoData()
	{
		global $easy_metadata;

   	    if(!$easy_metadata['data']) return; // No Config File Return

   	    easy_import_after_xml();

		//EASYFInstallerHelper::setMenus();
		EASYFInstallerHelper::setOptions();
		EASYFInstallerHelper::setMediaData();
		EASYFInstallerHelper::setWidgets();
		EASYFInstallerHelper::setHomePage();
		set_demo_menus();

		easy_import_end();

	}

	/**
	 * Add menus
	 *
	 * @since 0.0.1
	 */
	function set_demo_menus(){

		// Menus to Import and assign - you can remove or add as many as you want
		$main_menu   = get_term_by('name', 'Blog menu', 'nav_menu');

		set_theme_mod( 'nav_menu_locations', array(
				'primary' => $main_menu->term_id,
			)
		);
	}

	if(isset($_GET['page']) && $_GET['page'] == 'easint' ) 
	add_action('import_end','addMetaDemoData');
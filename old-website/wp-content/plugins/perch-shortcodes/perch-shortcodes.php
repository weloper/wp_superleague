<?php
/*
  Plugin Name: Perch Shortcodes
  Plugin URI: http://themeforest.net/user/themeperch/portfolio/ref=themeperch
  Version: 1.7.2
  Author: Themeperch
  Author URI: http://themeforest.net/user/themeperch/ref=themeperch
  Description: Supercharge your WordPress theme with mega pack of shortcodes
  Text Domain: perch
  Domain Path: /languages
  License: GPL
 */

// Define plugin constants
define( 'TP_PLUGIN_FILE', __FILE__ );
define( 'TP_PLUGIN_VERSION', '1.7.2' );
define( 'TP_ENABLE_CACHE', false );

// Includes
require_once 'inc/vendor/perch.php';
require_once 'inc/core/admin-views.php';
require_once 'inc/core/requirements.php';
require_once 'inc/core/load.php';
require_once 'inc/core/assets.php';
require_once 'inc/core/shortcodes.php';
require_once 'inc/core/tools.php';
require_once 'inc/core/data.php';
require_once 'inc/core/generator-views.php';
//require_once 'inc/core/generator.php';
//require_once 'inc/core/widget.php';

if ( ! function_exists('electron_performer_post_type') ) {

// Register Custom Post Type
function electron_performer_post_type() {

    $labels = array(
        'name'                  => _x( 'Performers', 'Post Type General Name', 'electron' ),
        'singular_name'         => _x( 'Performer', 'Post Type Singular Name', 'electron' ),
        'menu_name'             => __( 'Performer', 'electron' ),
        'name_admin_bar'        => __( 'Performers', 'electron' ),
        'archives'              => __( 'Performer Archives', 'electron' ),
        'parent_item_colon'     => __( 'Parent Performer:', 'electron' ),
        'all_items'             => __( 'All Performers', 'electron' ),
        'add_new_item'          => __( 'Add New Performer', 'electron' ),
        'add_new'               => __( 'Add New', 'electron' ),
        'new_item'              => __( 'New Performer', 'electron' ),
        'edit_item'             => __( 'Edit Performer', 'electron' ),
        'update_item'           => __( 'Update Performer', 'electron' ),
        'view_item'             => __( 'View Performer', 'electron' ),
        'search_items'          => __( 'Search Performer', 'electron' ),
        'not_found'             => __( 'Not found', 'electron' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'electron' ),
        'featured_image'        => __( 'Featured Image', 'electron' ),
        'set_featured_image'    => __( 'Set featured image', 'electron' ),
        'remove_featured_image' => __( 'Remove featured image', 'electron' ),
        'use_featured_image'    => __( 'Use as featured image', 'electron' ),
        'insert_into_item'      => __( 'Insert into item', 'electron' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'electron' ),
        'items_list'            => __( 'Performers list', 'electron' ),
        'items_list_navigation' => __( 'Performers list navigation', 'electron' ),
        'filter_items_list'     => __( 'Filter items list', 'electron' ),
    );
    $args = array(
        'label'                 => __( 'Performer', 'electron' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
        'hierarchical'          => true,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,        
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'performer', $args );

}
add_action( 'init', 'electron_performer_post_type', 0 );

}

if ( ! function_exists('electron_event_facilities_post_type') ) {

// Register Custom Post Type
function electron_event_facilities_post_type() {

    $labels = array(
        'name'                  => _x( 'Event Facilities', 'Post Type General Name', 'electron' ),
        'singular_name'         => _x( 'Event Facility', 'Post Type Singular Name', 'electron' ),
        'menu_name'             => __( 'Event Facilities', 'electron' ),
        'name_admin_bar'        => __( 'Event Facilities', 'electron' ),
        'archives'              => __( 'Event Facility Archives', 'electron' ),
        'parent_item_colon'     => __( 'Event Facility:', 'electron' ),
        'all_items'             => __( 'Event Facilities', 'electron' ),
        'add_new_item'          => __( 'Add New Facility', 'electron' ),
        'add_new'               => __( 'Add New', 'electron' ),
        'new_item'              => __( 'New Event Facility', 'electron' ),
        'edit_item'             => __( 'Edit Event Facility', 'electron' ),
        'update_item'           => __( 'Update Event Facility', 'electron' ),
        'view_item'             => __( 'View Event Facility', 'electron' ),
        'search_items'          => __( 'Search Event Facility', 'electron' ),
        'not_found'             => __( 'Not found', 'electron' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'electron' ),
        'featured_image'        => __( 'Featured Image', 'electron' ),
        'set_featured_image'    => __( 'Set featured image', 'electron' ),
        'remove_featured_image' => __( 'Remove featured image', 'electron' ),
        'use_featured_image'    => __( 'Use as featured image', 'electron' ),
        'insert_into_item'      => __( 'Insert into item', 'electron' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'electron' ),
        'items_list'            => __( 'Performers list', 'electron' ),
        'items_list_navigation' => __( 'Performers list navigation', 'electron' ),
        'filter_items_list'     => __( 'Filter items list', 'electron' ),
    );
    $args = array(
        'label'                 => __( 'Event Facilities', 'electron' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'page-attributes' ),
        'hierarchical'          => true,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,        
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'facility', $args );

}
add_action( 'init', 'electron_event_facilities_post_type', 1 );

}

function perch_get_terms( $tax = 'category', $key = 'id' ) {
    $terms = array();
    if ( $key === 'id' ) foreach ( (array) get_terms( $tax, array( 'hide_empty' => false ) ) as $term ) $terms[$term->term_id] = $term->name;
      elseif ( $key === 'slug' ) foreach ( (array) get_terms( $tax, array( 'hide_empty' => false ) ) as $term ) $terms[$term->slug] = $term->name;
        return $terms;
}
if(function_exists('add_shortcode_param')){
  vc_add_shortcode_param( 'number', 'perch_vc_number_settings_field' );
  vc_add_shortcode_param( 'perch_select', 'perch_vc_perch_select_settings_field' );
  vc_add_shortcode_param( 'image_upload', 'perch_vc_image_upload_settings_field' );
}
function perch_vc_number_settings_field( $settings, $value ) {
   return '<div class="my_param_block">'
             .'<input name="' . esc_attr( $settings['param_name'] ) . '" class="wpb_vc_param_value wpb-textinput ' .
             esc_attr( $settings['param_name'] ) . ' ' .
             esc_attr( $settings['type'] ) . '_field" type="number" min="'.intval($settings['min']).'" max="'.intval($settings['max']).'" step="'.intval($settings['step']).'" value="' . esc_attr( $value ) . '" />' .
             '</div>'; // This is html markup that will be outputted in content elements edit form
}
function perch_vc_image_upload_settings_field($settings, $value){
  return '<div class="bp-upload-container">
      <input type="text" name="' . esc_attr( $settings['param_name'] ) . '" value="'.esc_url($value).'" class="wpb_vc_param_value wpb-textinput perch-generator-attr perch-generator-upload-value" />
      <a href="javascript:;" class="button electron-upload-button"><span class="wp-media-buttons-icon"></span>'.__( 'Media manager', 'electron' ).'</a>
      <img width="80" src="'.esc_url($value).'" alt="Image URL">     
    </div>';
}
function perch_vc_registered_sidebar(){
    $sidebars = array();
   /* get the registered sidebars */
    global $wp_registered_sidebars;

    $sidebars = array();
    foreach( $wp_registered_sidebars as $id=>$sidebar ) {
      $sidebars[ $id ] = $sidebar[ 'name' ];
    }
    return $sidebars;    
}
function perch_vc_perch_select_settings_field( $args, $value ) {
    $selected = is_array($value)? $value : explode(',', $value);
    $args = wp_parse_args( $args, array(
        'param_name'       => '',
        'heading'     => '',
        'class'    => 'wpb_vc_param_value wpb-input wpb-select dropdown',
        'multiple' => '',
        'size'     => '',
        'disabled' => '',
        'selected' => $selected,
        'none'     => '',
        'value'  => array(),
        'style' => '',
        'format'   => 'keyval', // keyval/idtext
        'noselect' => '' // return options without <select> tag
      ) );
    $options = array();
    if ( !is_array( $args['value'] ) ) $args['value'] = array();
     if ( $args['param_name'] ) $name = ' name="' . $args['param_name'] . '"';
    if ( $args['param_name'] ) $args['param_name'] = ' id="' . $args['param_name'] . '"';   
    if ( $args['class'] ) $args['class'] = ' class="' . $args['class'] . '"';
    if ( $args['style'] ) $args['style'] = ' style="' . esc_attr( $args['style'] ) . '"';
    if ( $args['multiple'] ) $args['multiple'] = ' multiple="multiple"';
    if ( $args['disabled'] ) $args['disabled'] = ' disabled="disabled"';
    if ( $args['size'] ) $args['size'] = ' size="' . $args['size'] . '"';
    if ( $args['none'] && $args['format'] === 'keyval' ) $args['options'][0] = $args['none'];
    if ( $args['none'] && $args['format'] === 'idtext' ) array_unshift( $args['options'], array( 'id' => '0', 'text' => $args['none'] ) );
    
    // keyval loop
    // $args['options'] = array(
    //   id => text,
    //   id => text
    // );
    if ( $args['format'] === 'keyval' ) foreach ( $args['value'] as $id => $text ) {
        $options[] = '<option value="' . (string) $id . '">' . (string) $text . '</option>';
      }
    // idtext loop
    // $args['options'] = array(
    //   array( id => id, text => text ),
    //   array( id => id, text => text )
    // );
    elseif ( $args['format'] === 'idtext' ) foreach ( $args['options'] as $option ) {
        if ( isset( $option['id'] ) && isset( $option['text'] ) )
          $options[] = '<option value="' . (string) $option['id'] . '">' . (string) $option['text'] . '</option>';
      }
    $options = implode( '', $options );

    if(is_array($args['selected'])){
        foreach ($args['selected'] as $key => $value) {
          $options = str_replace( 'value="' . $value . '"', 'value="' . $value . '" selected="selected"', $options );
        }
    }else{
      $options = str_replace( 'value="' . $args['selected'] . '"', 'value="' . $args['selected'] . '" selected="selected"', $options );
    }
    
    $output = ( $args['noselect'] ) ? $options : '<select' .$name. $args['param_name'] . $args['class'] . $args['multiple'] . $args['size'] . $args['disabled'] . $args['style'] . '>' . $options . '</select>';
   // $output .= '<input type="hidden" '.$name.' value="'.$value.'">';
    return '<div class="perch_select_param_block">'.$output.'</div>';
}

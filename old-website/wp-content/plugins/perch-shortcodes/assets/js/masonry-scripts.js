jQuery(document).ready(function($){

	// vanilla JS
	var msnry = new Masonry( '.bohopeople-posts-masonary-loop', {
	  	columnWidth: 200,
	  	itemSelector: '.masonary-post',
		gutter: 10,
		percentPosition: true
	});
})
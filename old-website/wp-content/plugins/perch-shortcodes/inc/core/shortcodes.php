<?php
function perch_posts_template( $atts = null, $content = null, $type="posts" ) {
	// Prepare error var
	$error = null;
	// Parse attributes
	$atts = shortcode_atts( array(
			'template'            => 'templates/default-loop.php',
			'id'                  => false,
			'posts_per_page'      => get_option( 'posts_per_page' ),
			'post_type'           => 'post',
			'taxonomy'            => 'category',
			'tax_term'            => false,
			'tax_operator'        => 'IN',
			'author'              => '',
			'tag'                 => '',
			'meta_key'            => '',
			'offset'              => 0,
			'order'               => 'DESC',
			'orderby'             => 'date',
			'post_parent'         => false,
			'post_status'         => 'publish',
			'ignore_sticky_posts' => 'no',
			'autoplay' => 'no',
			'control' => 'yes',
			'column'			  => 4,
		), $atts, $type );

	$original_atts = $atts;

	$author = sanitize_text_field( $atts['author'] );
	$id = $atts['id']; // Sanitized later as an array of integers
	$ignore_sticky_posts = ( bool ) ( $atts['ignore_sticky_posts'] === 'yes' ) ? true : false;
	$meta_key = sanitize_text_field( $atts['meta_key'] );
	$offset = intval( $atts['offset'] );
	$order = sanitize_key( $atts['order'] );
	$orderby = sanitize_key( $atts['orderby'] );
	$post_parent = $atts['post_parent'];
	$post_status = $atts['post_status'];
	$post_type = sanitize_text_field( $atts['post_type'] );
	$posts_per_page = intval( $atts['posts_per_page'] );
	$tag = sanitize_text_field( $atts['tag'] );
	$tax_operator = $atts['tax_operator'];
	$tax_term = sanitize_text_field( $atts['tax_term'] );
	$taxonomy = sanitize_key( $atts['taxonomy'] );
	// Set up initial query for post
	$args = array(
		'category_name'  => '',
		'order'          => $order,
		'orderby'        => $orderby,
		'post_type'      => explode( ',', $post_type ),
		'posts_per_page' => $posts_per_page,
		'tag'            => $tag
	);
	// Ignore Sticky Posts
	if ( $ignore_sticky_posts ) $args['ignore_sticky_posts'] = true;
	// Meta key (for ordering)
	if ( !empty( $meta_key ) ) $args['meta_key'] = $meta_key;
	// If Post IDs
	if ( $id ) {
		$posts_in = array_map( 'intval', explode( ',', $id ) );
		$args['post__in'] = $posts_in;
	}
	// Post Author
	if ( !empty( $author ) ) $args['author'] = $author;
	// Offset
	if ( !empty( $offset ) ) $args['offset'] = $offset;
	// Post Status
	$post_status = explode( ', ', $post_status );
	$validated = array();
	$available = array( 'publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash', 'any' );
	foreach ( $post_status as $unvalidated ) {
		if ( in_array( $unvalidated, $available ) ) $validated[] = $unvalidated;
	}
	if ( !empty( $validated ) ) $args['post_status'] = $validated;
	// If taxonomy attributes, create a taxonomy query
	if ( !empty( $taxonomy ) && !empty( $tax_term ) ) {
		// Term string to array
		$tax_term = explode( ',', $tax_term );
		// Validate operator
		if ( !in_array( $tax_operator, array( 'IN', 'NOT IN', 'AND' ) ) ) $tax_operator = 'IN';
		$tax_args = array( 'tax_query' => array( array(
					'taxonomy' => $taxonomy,
					'field' => ( is_numeric( $tax_term[0] ) ) ? 'id' : 'slug',
					'terms' => $tax_term,
					'operator' => $tax_operator ) ) );
		// Check for multiple taxonomy queries
		$count = 2;
		$more_tax_queries = false;
		while ( isset( $original_atts['taxonomy_' . $count] ) && !empty( $original_atts['taxonomy_' . $count] ) &&
			isset( $original_atts['tax_' . $count . '_term'] ) &&
			!empty( $original_atts['tax_' . $count . '_term'] ) ) {
			// Sanitize values
			$more_tax_queries = true;
			$taxonomy = sanitize_key( $original_atts['taxonomy_' . $count] );
			$terms = explode( ', ', sanitize_text_field( $original_atts['tax_' . $count . '_term'] ) );
			$tax_operator = isset( $original_atts['tax_' . $count . '_operator'] ) ? $original_atts[
			'tax_' . $count . '_operator'] : 'IN';
			$tax_operator = in_array( $tax_operator, array( 'IN', 'NOT IN', 'AND' ) ) ? $tax_operator : 'IN';
			$tax_args['tax_query'][] = array( 'taxonomy' => $taxonomy,
				'field' => 'slug',
				'terms' => $terms,
				'operator' => $tax_operator );
			$count++;
		}
		if ( $more_tax_queries ):
			$tax_relation = 'AND';
		if ( isset( $original_atts['tax_relation'] ) &&
			in_array( $original_atts['tax_relation'], array( 'AND', 'OR' ) )
		) $tax_relation = $original_atts['tax_relation'];
		$args['tax_query']['relation'] = $tax_relation;
		endif;
		$args = array_merge( $args, $tax_args );
	}

	// Fix for pagination
	if( is_front_page() ) { 
		$paged = ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1; 
	} else { 
		$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1; 
	}
	$args['paged'] = $paged;

	// If post parent attribute, set up parent
	if ( $post_parent ) {
		if ( 'current' == $post_parent ) {
			global $post;
			$post_parent = $post->ID;
		}
		$args['post_parent'] = intval( $post_parent );
	}
	// Save original posts
	global $posts;
	$original_posts = $posts;


	// Query posts
	$posts = new WP_Query( $args );

	$posts->autoplay = $atts['autoplay'];
	$posts->column = $atts['column'];
	$posts->control = $atts['control'];	

	// Buffer output
	ob_start();
	// Search for template in stylesheet directory
	if ( file_exists( STYLESHEETPATH . '/' . $atts['template'] ) ) load_template( STYLESHEETPATH . '/' . $atts['template'], false );
	// Search for template in theme directory
	elseif ( file_exists( TEMPLATEPATH . '/' . $atts['template'] ) ) load_template( TEMPLATEPATH . '/' . $atts['template'], false );
	// Search for template in plugin directory
	elseif ( path_join( dirname( TP_PLUGIN_FILE ), $atts['template'] ) ) load_template( path_join( dirname( TP_PLUGIN_FILE ), $atts['template'] ), false );
	// Template not found
	else echo Tp_Tools::error( __FUNCTION__, __( 'template not found', 'perch' ) );
	$output = ob_get_contents();
	ob_end_clean();
	// Return original posts
	$posts = $original_posts;
	// Reset the query
	wp_reset_postdata();

	return $output;
}
class Tp_Shortcodes {
	static $tabs = array();
	static $tab_count = 0;

	function __construct() {
		add_shortcode( 'perch_home_slider',               		array( __CLASS__, 'home_slider' ) );
		add_shortcode( 'perch_special_guests',                	array( __CLASS__, 'special_guests' ) );
		add_shortcode( 'perch_event_info',                		array( __CLASS__, 'event_info' ) );
		add_shortcode( 'perch_about_us',                   		array( __CLASS__, 'about_us' ) );
		add_shortcode( 'perch_feature_box',                   	array( __CLASS__, 'feature_box' ) );
		add_shortcode( 'perch_testimonial_slider',              array( __CLASS__, 'testimonial_slider' ) );
		add_shortcode( 'perch_sponsors_slider',                 array( __CLASS__, 'sponsors_slider' ) );
		add_shortcode( 'perch_performers',                   	array( __CLASS__, 'performers' ) );
		add_shortcode( 'perch_facilities',                   	array( __CLASS__, 'facilities' ) );
		add_shortcode( 'perch_posts_template',                  array( __CLASS__, 'posts_template' ) );
		add_shortcode( 'perch_highlights',                   	array( __CLASS__, 'highlights' ) );
		add_shortcode( 'perch_offer',                   		array( __CLASS__, 'offer' ) );
		add_shortcode( 'perch_pricing',                   		array( __CLASS__, 'pricing' ) );
		add_shortcode( 'perch_event_location',                  array( __CLASS__, 'event_location' ) );
		add_shortcode( 'perch_newsletter_form',                 array( __CLASS__, 'newsletter_form' ) );
		add_shortcode( 'perch_social_icons',                   	array( __CLASS__, 'social_icons' ) );
		add_shortcode( 'perch_comming_soon',                   	array( __CLASS__, 'comming_soon' ) );
	}

	public static function heading( $atts = null, $content = null ) {
		$atts = shortcode_atts( array(
				'style'  => 'default',
				'title'  => 'Heading title',
				'size'   => 3,
				'align'  => 'center',
				'margin' => '20',
				'class'  => ''
			), $atts, 'heading' );
		perch_query_asset( 'css', 'perch-content-shortcodes' );
		do_action( 'perch/shortcode/heading', $atts );
		return '<div class="perch-heading perch-heading-style-' . $atts['style'] . ' perch-heading-align-' . $atts['align'] . perch_ecssc( $atts ) . '" style="margin-bottom:' . $atts['margin'] . 'px"><h'. $atts['size'] .' class="perch-heading-inner">' . esc_attr($atts['title']) . '</h'. $atts['size'] .'><div class="heading-subtitle">'.do_shortcode( $content ).'</div></div>';
	}

	public static function alert( $atts = null, $content = null ) {
		$atts = shortcode_atts( array(
				'style'  => 'success',
				'size'   => 3,
				'align'  => 'left',
				'color'  => 'no',
				'margin' => '20',
				'title'  => 'Heading title'
			), $atts, 'alert' );
		perch_query_asset( 'css', 'bootstrap-theme' );
		perch_query_asset( 'css', 'perch-content-shortcodes' );
		do_action( 'perch/shortcode/alert', $atts );
		return '<div class="alert alert-'. $atts['style'] .' color-'. $atts['color'] .' text-'. $atts['align'] .'" role="alert" style="margin-bottom:' . $atts['margin'] . 'px"><h'. $atts['size'] .' class="alert-heading">'. $atts['title'] .'</h'. $atts['size'] .'>' . do_shortcode( $content ) . '</div>';
	}

	
	public static function home_slider( $atts = null, $content = null ) {
		$atts = shortcode_atts( array(
				'slides' => '',
				'button_text' => 'Join the Event',
				'button_link' => '',
				'countdown_time' => 'August 7, 2017 22:00:00',
				'slide_shape' => 'yes',
				'down_arrow' => 'yes',
			), $atts, 'home_slider' );

		do_action( 'perch/shortcode/home_slider', $atts );
		$output = '';
		$slides = (function_exists('vc_param_group_parse_atts'))? vc_param_group_parse_atts($atts['slides']) : array(
				array(
					'title' => __( 'FEEL THE ENERGY', 'electron' ),
					'value' => 'BREATHTAKING CROWD',
					'bg_image' => 'http://www.dezinethemes.com/envato/music/01/img/slider/slide1.jpg',
					'desc' => '',
				),
				array(
					'title' => __( 'Shake it out', 'electron' ),
					'subtitle' => 'Rock solid sounds',
					'desc' => '',
					'bg_image' => 'http://www.dezinethemes.com/envato/music/01/img/slider/slide2.jpg'
				),
				array(
					'title' => __( 'Electronic Madness', 'electron' ),
					'subtitle' => 'Thrilling performances',
					'desc' => '',
					'bg_image' => 'http://www.dezinethemes.com/envato/music/01/img/slider/slide3.jpg'
				),
				);


		$slidesID = (count($slides) > 1)? 'slides' : 'slide-single';
		$output .= '<div id="'.esc_attr($slidesID).'">';

		$output .= '<div class="slides-container">';
		$i = 1;
		foreach ($slides as $key => $value) {
			$output .= '<div class="slide'.(($i == 1)? ' active' : '').'">
                    <div class="img"><img src="'.esc_url($value['bg_image']).'" alt="'.esc_attr($value['title']).'" class="img-responsive"></div>
                    <div class="slide-caption">
                        <div class="container">
                            <div class="box">
                            '.(($value['title'] != '')? '<h1>'.esc_attr($value['title']).'</h1>' : '').'
                            '.(($value['subtitle'] != '')? '<span>'.esc_attr($value['subtitle']).'</span>' : '').'
                            '.(($value['desc'] != '')? '<br><span style="margin-top: 0">'.esc_attr($value['desc']).'</span>' : '').'
                            </div>
                        </div>
                    </div>
                </div><!-- end slide1 -->';
            $i++;    
		}
		$output .= '</div>';

		$output .= '<div class="slides-navigation">
                <a class="prev sqaureIconSec" href="#"> <i class="fa fa-chevron-left"></i></a>
                <a class="next sqaureIconSec" href="#"> <i class="fa fa-chevron-right"></i></a>
            </div><!-- end slides-navigation -->';

        $output .= ($atts['slide_shape'] == 'yes')? '<div class="shapes absShape">
                <div class="shape1 absShape"></div>
                <div class="shape2 absShape"></div>
                <!-- <div class="gradient absShape"></div>-->
            </div>' : ''; 

        $output .= ($atts['down_arrow'] == 'yes')? '<div class="holder"><i class="fa fa-chevron-down moreArrow moving"></i></div>' : '';      

        $output .= '<div class="container-fluid absShape">
                '.(($atts['countdown_time'] != '')? '<div class="BGprime first col-md-6 col-sm-6 col-xs-12">
                    <div class="countdown styled" data-countdown-time="'.$atts['countdown_time'].'"></div>
                </div>' : '').'
                '.(($atts['button_text'] != '')? '<div class="BGsecondary col-md-6 col-sm-6 col-xs-12">
                    <a class="btn join smooth" href="'.$atts['button_link'].'" title="Join the event">'.esc_attr($atts['button_text']).'</a>
                </div>' : '').'
                
            </div>';

		$output .= '</div>';

		
		return $output;
	}

	public static function comming_soon( $atts = null, $content = null ) {
		$atts = shortcode_atts( array(
				'bg_image' => get_template_directory_uri().'/img/slide1.jpg',
				'title' => 'coming soon ! ',
				'desc' => 'Please check back again within Some Days as We\'re Pretty Close ',
				'countdown_time' => 'August 7, 2017 22:00:00',
				'slide_shape' => 'yes',
			), $atts, 'comming_soon' );

		do_action( 'perch/shortcode/comming_soon', $atts );
		$logo = (function_exists('ot_get_option'))? ot_get_option(  'logo', get_template_directory_uri().'/img/company_color.png' ) : get_template_directory_uri().'/img/company_color.png';
		$output = '<!--  :::  HOME SLIDER ::: -->
        <div class="coming-soon" style="background-image:url('.esc_url($atts['bg_image']).')">

            <div class="container text-center">
                <div class="page-block countdown-wrap">
                

                    <div class="logo text-center">
		                <a title="'.get_bloginfo( 'name' ).'" href="'.esc_url( home_url( '/' ) ).'" rel="home">
		                    <img class="img-responsive" src="'.esc_url($logo).'" alt="'.get_bloginfo( 'name' ).'" /></a>
		            </div>

                    <div class="page-block">
                        <h2>'.esc_attr($atts['title']).'</h2>
                        <hr>
                        <p class="big">'.esc_attr($atts['desc']).'</p>
                        <hr>
                    </div>
                    '.(($atts['countdown_time'] != '')? '<div class="BGprime first col-md-12 col-sm-12 col-xs-12">
                    <div class="countdown styled" data-countdown-time="'.$atts['countdown_time'].'"></div>
                </div>' : '').'


                </div>
            </div>
            '.(($atts['slide_shape'] == 'yes')? '<div class="shapes absShape">
                <div class="shape1 absShape"></div>
                <div class="shape2 absShape"></div>
                <!-- <div class="gradient absShape"></div>-->
            </div>' : '').'

        </div>
        <!-- ::: END ::: -->';
		
		return $output;
	}


	public static function special_guests( $atts = null, $content = null ) {
		$atts = shortcode_atts( array(
				'image' => '//www.dezinethemes.com/envato/music/01/img/special_guest.jpg',
				'connector_icon' =>'fa fa-star',
				'title'  => 'Special Guest Performance',
				'guest_info' => '',
			), $atts, 'special_guests' );
		
		do_action( 'perch/shortcode/special_guests', $atts );
		$atts['image'] = (is_numeric($atts['image']))? wp_get_attachment_url($atts['image']) : $atts['image'];
		$imageurl = ($atts['image'] != '')? $atts['image'] : 'http://www.dezinethemes.com/envato/music/01/css/../img/special_guest.jpg';

		$guest_info_arr = (function_exists('vc_param_group_parse_atts'))? vc_param_group_parse_atts($atts['guest_info']) : array();

		$guests = '';
		if( !empty($guest_info_arr)){
			foreach ($guest_info_arr as $key => $value) {
				$guests .= ' <h4><i class="'.$value['guest_icon'].'"></i>'.esc_attr($value['title']).'</h4>
		          <strong>'.esc_attr($value['subtitle']).'</strong>
		          <hr class="light">';
			}
		}

		return '<div class="guest" style="background-image: url('.esc_url($imageurl).')">
		    <div class="row">
		    
		        <div class="col-lg-6 col-lg-offset-6 col-md-5 col-md-offset-7 col-sm-12 col-xs-12 BGprime page-block-full">
		        
		          <h3 class="white">'.$atts['title'].'</h3>'.$guests.'
		          <div class="white">'.do_shortcode($content).'</div>
		          <div class="sqaureIconPrime"><i class="'.$atts['connector_icon'].'"></i></div>
		            
		        </div>
		        
		    </div><!-- end row -->
		  </div>';
	}

	public static function testimonial_slider( $atts = null, $content = null ) {
		$atts = shortcode_atts( array(
				'testimonials_icon' => 'fa fa-quote-left',		
				'testimonials' => '',
			), $atts, 'testimonial_slider' );
		
		do_action( 'perch/shortcode/testimonial_slider', $atts );
		
		$testimonials_arr = (function_exists('vc_param_group_parse_atts'))? vc_param_group_parse_atts($atts['testimonials']) : array();

		$items = '';
		$indicators = '';
		$i = 0;
		if( !empty($testimonials_arr)){
			foreach ($testimonials_arr as $key => $value) {
				$items .= '<div class="item'.(($i == 0)? ' active': '').'">
                                <h4>'.esc_attr($value['title']).'</h4>
                                <p>'.esc_attr($value['description']).'</p>
                                <span><strong>'.esc_attr($value['name']).'</strong>'.esc_attr($value['position']).'</span>

                            </div><!-- end item -->';
                $indicators .= '<li data-target="#testimonial-slider" data-slide-to="'.$i.'"'.(($i == 0)? ' class="active"': '').'></li>';
                $i++;
			}
		}

		return '<div class="quotes text-center"><div class="sqaureIconSec"><i class="'.$atts['testimonials_icon'].'"></i></div>

                    <div id="testimonial-slider" class="carousel slide carousel-fade" data-ride="carousel">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner text-center">
                        '.$items.'
                        <!-- Indicators -->
                            <ol class="carousel-indicators">
                               '.$indicators.'
                            </ol>

                        </div><!-- end carousel-inner -->
                    </div><!-- end testimonial-slider --></div>';
	}

	public static function performers( $atts = null, $content = null ) {
		$atts = shortcode_atts( array(
				'title' => 'EVENT PERFORMERS',		
				'template' => 'templates/performer-carousel.php',
				'display' => 'all',
				'performers' => '',
				'column' => 4,
				'autoplay' => 'no',
				'control' => 'yes'
			), $atts, 'performers' );
		do_action( 'perch/shortcode/performers', $atts );

		$performers_arr = (function_exists('vc_param_group_parse_atts'))? vc_param_group_parse_atts($atts['performers']) : array();
		$arr = array();
		if(!empty($performers_arr)){
			foreach ($performers_arr as $key => $value) {
				$arr[] = $value['performer'];
			}
		}
		
		$atts['posts_per_page'] = ($atts['display'] == 'all')? -1: '';
		$atts['id'] = ($atts['display'] == 'specific')? implode(',', $arr): false;
		$atts['post_type'] = 'performer';

		$output = ($atts['title'] != '')? '<h2>'.esc_attr($atts['title']).'</h2>' : '';
		$output .= perch_posts_template($atts, '', 'performers');

		return $output;

	}

	public static function facilities( $atts = null, $content = null ) {
		$atts = shortcode_atts( array(
				'template' => 'templates/event-facilities-tabs.php',
				'display' => 'all',
				'facilities' => '',
				'column' => 4,
				'autoplay' => 'no',
				'control' => 'yes'
			), $atts, 'performers' );
		do_action( 'perch/shortcode/facilities', $atts );

		$facilities_arr = (function_exists('vc_param_group_parse_atts'))? vc_param_group_parse_atts($atts['facilities']) : array();
		$arr = array();
		if(!empty($facilities_arr)){
			foreach ($facilities_arr as $key => $value) {
				$arr[] = $value['facility'];
			}
		}
		
		$atts['posts_per_page'] = ($atts['display'] == 'all')? -1: '';
		$atts['id'] = ($atts['display'] == 'specific')? implode(',', $arr): false;
		$atts['post_type'] = 'facility';

		
		return perch_posts_template($atts, '', 'facilities');

	}

	public static function posts_template( $atts = null, $content = null ) {
		$atts = shortcode_atts( array(
				'template' => 'templates/blog-carousel.php',
				'display' => 'all',
				'category' => '',
				'posts' => '',
				'column' => 4,
				'autoplay' => 'no',
				'posts_per_page' => 10,
				'control' => 'yes'
			), $atts, 'posts_template' );
		do_action( 'perch/shortcode/posts_template', $atts );

		$posts_arr = (function_exists('vc_param_group_parse_atts'))? vc_param_group_parse_atts($atts['posts']) : array();
		$arr = array();
		if(!empty($posts_arr)){
			foreach ($posts_arr as $key => $value) {
				$arr[] = $value['post'];
			}
		}
		
		$atts['posts_per_page'] = ($atts['display'] == 'all')? -1: $atts['posts_per_page'];
		$atts['id'] = ($atts['display'] == 'specific')? implode(',', $arr): false;
		$atts['tax_term'] = ($atts['display'] == 'category')? $atts['category']: false;

		
		return perch_posts_template($atts, '', 'posts_template');

	}

	public static function sponsors_slider( $atts = null, $content = null ) {
		$atts = shortcode_atts( array(
				'title' => 'Sponsors',		
				'sponsors' => '',
				'column' => 4,
				'autoplay' => 'no',
				'control' => 'yes'
			), $atts, 'sponsors_slider' );
		
		do_action( 'perch/shortcode/sponsors_slider', $atts );
		
		$sponsors_arr = (function_exists('vc_param_group_parse_atts'))? vc_param_group_parse_atts($atts['sponsors']) : array();

		$items = '';
		if( !empty($sponsors_arr)){
			foreach ($sponsors_arr as $key => $value) {
				$items .= ($value['image'] != '')? '<div class="item">
                                <img src="'.esc_url($value['image']).'" alt="'.esc_attr($value['title']).'">
                            </div>' : '';
			}
		}

		return (($atts['title'] != '')? '<h2>'.esc_attr($atts['title']).'</h2>' : '').'
		<div id="sponsorOwl" class="owl-carousel sponsorOwl" data-column="'.esc_attr($atts['column']).'" data-control="'.esc_attr($atts['control']).'" data-autoplay="'.esc_attr($atts['autoplay']).'">'.$items.'</div>';
	}

	public static function social_icons( $atts = null, $content = null ) {
		$atts = shortcode_atts( array(
				'title' => 'Questions?',		
				'social_links' => '',
			), $atts, 'social_icons' );
		
		do_action( 'perch/shortcode/social_icons', $atts );
		
		$social_links_arr = (function_exists('vc_param_group_parse_atts'))? vc_param_group_parse_atts($atts['social_links']) : array();

		$items = '';
		if( !empty($social_links_arr)){
			foreach ($social_links_arr as $key => $value) {
				$items .= ($value['link'] != '')? '<li><a target="_blank" href="'.esc_url($value['link']).'" data-toggle="tooltip" title="'.esc_attr($value['title']).'"><i class="'.esc_attr($value['icon']).'"></i></a></li>' : '';
			}
		}

		return '<div class="row text-center">'.(($atts['title'] != '')? '<div class="col-md-3 col-sm-12"><h2>'.esc_attr($atts['title']).'</h2></div>' : '').'
		<div class="col-md-9 col-sm-12 social"><ul class="list-inline text-center">'.$items.'</ul></div></div>';
	}

	public static function event_info( $atts = null, $content = null ) {
		$atts = shortcode_atts( array(
				'event_info_type' => 'custom',
				'tickera_event' => '',
				'date_icon' => 'fa fa-calendar',		
				'date_title' => 'Event Date & Time',
				'date' => '7 August 2017',
				'time' => '10.00 pm',
				'location_icon' => 'fa fa-map-marker',
				'location_title' => 'Event Location',
				'location' => '123 Main Street<br>San Francisco, CA 94105, USA',
				'button_icon' => 'fa fa-arrow-circle-o-right',
				'button_text' => 'Get Directions',
				'button_link' => '#',
			), $atts, 'event_info' );
		
		do_action( 'perch/shortcode/event_info', $atts );
		
		
		$datetime = '<strong>'.esc_attr($atts['date']).'</strong>'.(($atts['time'] != '')? '<br><span>@</span>'.esc_attr($atts['time']) : '');
		$location = $atts['location'];

		if ( class_exists( 'TC' ) ){
			if(($atts['tickera_event'] != '') && ($atts['event_info_type'] != 'custom')){
				$event_id = $atts['tickera_event'];
				$event_start_date	 = get_post_meta( $event_id, 'event_date_time', true );
				$event_end_date		 = get_post_meta( $event_id, 'event_end_date_time', true );

				$start_date	 = date_i18n( get_option( 'date_format' ), strtotime( $event_start_date ) );
				$start_time	 = date_i18n( get_option( 'time_format' ), strtotime( $event_start_date ) );

				$end_date	 = date_i18n( get_option( 'date_format' ), strtotime( $event_end_date ) );
				$end_time	 = date_i18n( get_option( 'time_format' ), strtotime( $event_end_date ) );

				if ( !empty( $event_end_date ) ) {
					if ( $start_date == $end_date ) {
						if ( $start_time == $end_time ) {
							$event_date = '<strong>'.$start_date . '</strong><br><span>@</span>' . $start_time;
						} else {
							$event_date = '<strong>'.$start_date . '</strong><br><span>@</span>' . $start_time . ' - ' . $end_time;
						}
					} else {
						if ( $start_time == $end_time ) {
							$event_date = '<strong>'.$start_date . '</strong> - <strong>' . $end_date . '</strong><br><span>@</span>' . $start_time;
						} else {
							$event_date = '<strong>'.$start_date . '</strong> <span>@</span> ' . $start_time . ' <br> <strong>' . $end_date . '</strong> <span>@</span> ' . $end_time;
						}
					}
				} else {
					$event_date = $start_date . '<br><span>@</span> ' . $start_time;
				}

				$datetime = $event_date;

				$event		 = new TC_Event( $event_id );
				$location = $event->details->event_location;


			}// if(($atts['tickera_event'] != '') && ($atts['event_info_type'] != 'custom'))
		}

		return '<div class="eventData text-center"><div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 prime">
                        <div class="dblBorder">
                            <div class="dblBorderChild BGdark">
                                <span class="sqaureIconPrime absolute"><i class="'.esc_attr($atts['date_icon']).' prime"></i></span>
                                '.(($atts['date_title'] != '')? '<h3>'.esc_attr($atts['date_title']).'</h3>' : '').'
                                <p class="big">'.$datetime.'</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 secondary">
                        <div class="dblBorder">
                            <div class="dblBorderChild BGdark">
                                <span class="sqaureIconSec absolute"><i class="'.esc_attr($atts['location_icon']).' prime"></i></span> 
                                '.(($atts['location_title'] != '')? '<h3>'.esc_attr($atts['location_title']).'</h3>' : '').'
                                <p class="big">'.$location.'</p>
                            </div>
                        </div>
                    </div>
                    '.(($atts['button_text'] != '')? '<div class="col-md-12 col-sm-12 col-xs-12">
                        <a class="btn smooth btn-lg" href="'.esc_attr($atts['button_link']).'" title="'.esc_attr($atts['button_text']).'"><i class="'.esc_attr($atts['button_icon']).'"></i> '.esc_attr($atts['button_text']).'</a>
                    </div>' : '').'
                </div><!-- end row --></div>';
	}

	public static function about_us( $atts = null, $content = null ) {
		$atts = shortcode_atts( array(
				'image' => '//www.dezinethemes.com/envato/music/01/img/band.png',
				'title'  => 'About us',
				'button_text' => 'Visit Our Website',
				'button_link' => '#',
				'button_icon' => 'fa fa-globe',
			), $atts, 'about_us' );
		
		do_action( 'perch/shortcode/about_us', $atts );


		return '<!-- ::: START ABOUT ::: -->
        <div class="about-section" style="background-image: url('.esc_url($atts['image']).')">
                <div class="row">        
                    <div class="col-md-5 col-md-offset-6 col-sm-8 col-sm-offset-4 col-xs-12">
                        '.(($atts['title'] != '')? '<h2>'.esc_attr($atts['title']).'</h2>' : '').'
                        <div class="big">'.do_shortcode($content).'</div>
                        '.(($atts['button_text'] != '')? '<a class="btn btn-default" href="'.$atts['button_link'].'" title="'.esc_attr($atts['title']).'"><i class="'.esc_attr($atts['button_icon']).'"></i> '.esc_attr($atts['button_text']).'</a>' : '').'
                       
                    </div>  
                </div><!-- end row -->
        </div>
        <!-- ::: END ::: -->';
	}

	public static function offer( $atts = null, $content = null ) {
		$atts = shortcode_atts( array(
				'large_color_text' => '30',
				'small_color_text' => '%',
				'small_text' => 'OFF',
				'title'  => 'Purchase tickets before 20th MARCH',
				'button_text' => 'Purchase Tickets',
				'button_link' => '#',
				'button_icon' => 'fa fa-ticket',
			), $atts, 'offer' );
		
		do_action( 'perch/shortcode/offer', $atts );


		return '<div class="row offer">
                    <div class="col-lg-4 col-md-5 col-sm-5 col-xs-12">
                        '.(($atts['large_color_text'] != '')? '<h1>'.esc_attr($atts['large_color_text']).'</h1>' : '').'
                        <h1 class="small">'.(($atts['small_color_text'] != '')? esc_attr($atts['small_color_text']) : '').'
                        '.(($atts['small_text'] != '')? '<span>'.esc_attr($atts['small_text']).'</span>' : '').'
                        </h1>
                    </div>
                    <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                        '.(($atts['title'] != '')? '<h3>'.esc_attr($atts['title']).'</h3>' : '').'
                        <div class="big">'.do_shortcode($content).'</div>
                        '.(($atts['button_text'] != '')? '<a class="btn btn-default smooth" href="'.$atts['button_link'].'" title="'.esc_attr($atts['title']).'"><i class="'.esc_attr($atts['button_icon']).'"></i> '.esc_attr($atts['button_text']).'</a>' : '').'
                    </div>

                </div><!-- end row -->';
	}

	public static function pricing( $atts = null, $content = null ) {
		$atts = shortcode_atts( array(
				'featured' => '',
				'pricing_type' => 'custom',
				'tickera_ticket' => '',
				'price_unit' => '$',
				'price' => '499',
				'small_text' => 'Group of 25',
				'button_text' => 'Purchase Tickets',
				'button_link' => '#',
				'eventbriteid' => '18432753863',
				'button_icon' => 'fa fa-ticket',
			), $atts, 'pricing' );
		
		do_action( 'perch/shortcode/pricing', $atts );

		$unit = esc_attr($atts['price_unit']);
		$price = esc_attr($atts['price']);
		$small_text = esc_attr($atts['small_text']);
		$details = do_shortcode($content);
		$purchase_button = (($atts['button_text'] != '')? '<a target="_blank" class="btn '.(($atts['featured'])? 'btn-dark' : 'btn-primary').' btn-sm" href="'.$atts['button_link'].'" title="'.esc_attr($atts['button_text']).'"><i class="'.esc_attr($atts['button_icon']).'"></i> '.esc_attr($atts['button_text']).'</a>' : '');

		if ( class_exists( 'TC' ) ){
			if(($atts['tickera_ticket'] != '') && ($atts['pricing_type'] == 'tickera')){
				$id = $atts['tickera_ticket'];
				$tc_general_settings = get_option( 'tc_general_setting', false );
				$unit = (isset( $tc_general_settings[ 'currency_symbol' ] ) && $tc_general_settings[ 'currency_symbol' ] != '' ? $tc_general_settings[ 'currency_symbol' ] : (isset( $tc_general_settings[ 'currencies' ] ) ? $tc_general_settings[ 'currencies' ] : '$'));
				$price = get_post_meta( $id, 'price_per_ticket', true );
				$small_text	= esc_attr(get_the_title($id));
				$ticket	= new TC_Ticket( apply_filters( 'tc_ticket_type_id', $id ) );
				$details = apply_filters( 'tc_ticket_description_element', apply_filters( 'tc_the_content', $ticket->details->post_content ) );
				if(function_exists('electron_ticket_cart_button')){
					$args = array(
							'id' => $id,
							'title' => $atts['button_text'],
							'featured' => $atts['featured'],
							'button_icon' => $atts['button_icon'],

						);
					$purchase_button = electron_ticket_cart_button($args);
				}
			}
		}//if ( class_exists( 'TC' ) )	


		if($atts['pricing_type'] == 'eventbrite'){
			$uniqid = 'register-'. $atts['eventbriteid'];
			$purchase_button = (($atts['button_text'] != '')? '<a data-toggle="modal" class="btn '.(($atts['featured'])? 'btn-dark' : 'btn-primary').' btn-sm" href="#'.$uniqid.'" title="'.esc_attr($atts['button_text']).'"><i class="'.esc_attr($atts['button_icon']).'"></i> '.esc_attr($atts['button_text']).'</a>' : '');
			global $wp_query;
			if(isset($wp_query->eventbrite) && empty($wp_query->eventbrite)){
				$wp_query->eventbrite = array( $uniqid => $atts['eventbriteid']);
			}else{
				$arr = $wp_query->eventbrite;
				$arr[$uniqid] = $atts['eventbriteid'];
				$wp_query->eventbrite = $arr;
			}

		}

		return '<div class="pricing"><div class="package '.(($atts['featured'])? 'BGprime' : 'BGdark').' text-center">
                            <div class="inner">
                                <h2>'.(($atts['price_unit'] != '')? '<sup>'.$unit.'</sup>' : '').$price.'</h2>
                                '.(($atts['small_text'] != '')? '<h6>'.$small_text.'</h6>' : '').'
                                <hr>
                                <div class="small">'.$details.'</div>
                                '.$purchase_button.'
                            </div>
                        </div></div>';
	}

	public static function highlights( $atts = null, $content = null ) {
		$atts = shortcode_atts( array(
				'title'  => 'Highlights',
				'bottom_title' => 'Check it out',
				'highlight_icon' => 'fa fa-chevron-down',
			), $atts, 'highlights' );
		
		do_action( 'perch/shortcode/hightlights', $atts );


		return '<div class="highlights text-center">'.(($atts['title'] != '')? '<h2>'.esc_attr($atts['title']).'</h2>' : '').'<hr>
                <div class="big">'.do_shortcode($content).'</div>
                <hr>
                '.(($atts['bottom_title'] != '')? '<h5>'.esc_attr($atts['bottom_title']).'</h5>' : '').'<br>
                <p><span class="sqaureIconSec"><i class="'.$atts['highlight_icon'].'"></i></span></p></div>';
	}

	public static function feature_box( $atts = null, $content = null ) {
		$atts = shortcode_atts( array(
				'feature_icon' => 'fa fa-ticket',
				'title'  => 'TICKETS',
				), $atts, 'feature_box' );
		
		do_action( 'perch/shortcode/feature_box', $atts );

		


		return '<div class="featureBox text-center">
                    <div class="sqaureIconPrime absolute"><i class="'.$atts['feature_icon'].'"></i></div>
                    <h5>'.esc_attr($atts['title']).'</h5>
                    <p>'.do_shortcode($content).'</p>
                </div>';
	}

	public static function newsletter_form( $atts = null, $content = null ) {
		$atts = shortcode_atts( array(
				'desc' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.  Aenean viverra eget felis quis elementum. ',
				'title'  => 'stay in touch for upcoming events',
				'email_placeholder' => 'Enter Your Email Address Here...'
				), $atts, 'newsletter_form' );
		
		do_action( 'perch/shortcode/newsletter_form', $atts );

		


		return '<script type="text/javascript">
//<![CDATA[
if (typeof newsletter_check !== "function") {
window.newsletter_check = function (f) {
    var re = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-]{1,})+\.)+([a-zA-Z0-9]{2,})+$/;
    if (!re.test(f.elements["ne"].value)) {
        alert("The email is not correct");
        return false;
    }
    for (var i=1; i<20; i++) {
    if (f.elements["np" + i] && f.elements["np" + i].required && f.elements["np" + i].value == "") {
        alert("");
        return false;
    }
    }
    if (f.elements["ny"] && !f.elements["ny"].checked) {
        alert("You must accept the privacy statement");
        return false;
    }
    return true;
}
}
//]]>
</script>

<div class="newsletter newsletter-subscription subscribe">
<div class="text-center">
    '.(($atts['title'] != '')? '<h3 class="subscribeHeading">'.esc_attr($atts['title']).'</h3>' : ''). 
    (($atts['desc'] != '')? '<p>'.esc_attr($atts['desc']).'</p>' : '').'
</div>
<form id="subscribeForm" method="post" action="'.get_permalink(get_the_ID()).'?na=s" onsubmit="return newsletter_check(this)">
<div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
<!-- email -->
<input class="newsletter-email form-control" type="email" name="ne" placeholder="'.esc_attr($atts['email_placeholder']).'" required>
<input class="newsletter-submit sqaureIconPrime" type="submit" value="&#xf067;"/>
</form>
</div>
</div>
'.do_shortcode($content);
               
	}

	public static function event_location( $atts = null, $content = null ) {
		$atts = shortcode_atts( array(
				'title'  => 'Event Location',
				'latitude' => '37.791649',
				'longitude' => '-122.394395',
				'address' => '',
				'direction_title' => 'Get Directions',
				'direction_from' => 'Destination From:',
				'direction_from_placeholder' => '50 California Street',
				'direction_via' => 'Via(Optional):',
				'travel_mode_title' => 'Travel mode:',
				'travel_mode_driving' => 'Driving',
				'travel_mode_bicylcing' => 'Bicylcing',
				'travel_mode_transport' => 'Public transport',
				'travel_mode_walking' => 'Walking',
				'button_text' => 'Get Directions',
				'direction_result_title' => 'Directions',
				), $atts, 'event_location' );
		
		do_action( 'perch/shortcode/event_location', $atts );

		wp_enqueue_script( 'electron-directions' );

		$address_arr = (function_exists('vc_param_group_parse_atts'))? vc_param_group_parse_atts($atts['address']) : array();

		$items = '';
		if( !empty($address_arr)){
			foreach ($address_arr as $key => $value) {
				$items .= '<li> <i class="'.$value['icon'].'"></i> '.(($value['title'] != '')? '<strong>'.esc_attr($value['title']).'</strong><br>': '').'
                               '. $value['desc'].'
                            </li>';
			}
		}

		$address = ($items != '')? '<div class="col-md-3 col-sm-3">
                        <div class="column-content">
                            <ul class="address">
                                '.$items.'
                            </ul>
                        </div>
                    </div>' : '';
            $tavel_mode_form = '<div class="col-sm-12">
            				<div class="directions-input">
                            '.(($atts['direction_result_title'] != '')? '<h4>'.esc_attr($atts['direction_result_title']).'</h4>' : '').'
                            <form action="/routebeschrijving" onSubmit="electron_calcRoute();
                                    return false;" id="routeForm">
                                <div class="col-sm-6">
                                    <label for="routeStart"><strong>'.esc_attr($atts['direction_from']).'</strong></label>
                                    <input type="text" id="routeStart" placeholder="'.esc_attr($atts['direction_from_placeholder']).'">
                                </div>
                                <div class="col-sm-6">
                                    <label for="routeVia"><strong>'.esc_attr($atts['direction_via']).'</strong> </label>
                                    <input type="text" id="routeVia" value="">
                                </div>
                                <div class="col-sm-8">
                                    <label>'.esc_attr($atts['travel_mode_title']).'</label>
                                    <div class="clearfix">
                                        <label class="radio-option" for="travelMode1">
                                            <input type="radio" name="travelMode" id="travelMode1" value="DRIVING" checked />
                                            '.esc_attr($atts['travel_mode_driving']).'
                                        </label>
                                        <label class="radio-option" for="travelMode2">
                                            <input type="radio" name="travelMode" id="travelMode2" value="BICYCLING" />
                                            '.esc_attr($atts['travel_mode_bicylcing']).'
                                        </label>
                                        <label class="radio-option" for="travelMode3">
                                            <input type="radio" name="travelMode" id="travelMode3" value="TRANSIT" />
                                            '.esc_attr($atts['travel_mode_transport']).'
                                        </label>
                                        <label class="radio-option last" for="travelMode4">
                                            <input type="radio" name="travelMode" id="travelMode4" value="WALKING" />
                                            '.esc_attr($atts['travel_mode_walking']).'
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <input type="submit" value="&#xf041; '.esc_attr($atts['button_text']).'" class="btn btn-primary">
                                </div>
                            </form>
                        </div></div>';  
        $directions_result = '<div class="col-sm-12 column directions-results">
                            <div class="column-content">
                                '.(($atts['direction_title'] != '')? '<h4>'.esc_attr($atts['direction_title']).'</h4>' : '').'
                                <div id="directionsPanel">'.do_shortcode($content).'</div>
                            </div>
                        </div>';                      


		return '<div id="venue" class="BGdark">
            <div class="container-wide">
                <div id="map_canvas" data-latitude="'.esc_attr($atts['latitude']).'" data-longitude="'.esc_attr($atts['longitude']).'"> </div>
            </div>
            <div class="container">
                <div class="row">

                    <div class="col-md-12 text-center">'.(($atts['title'] != '')? '<h2>'.esc_attr($atts['title']).'</h2>' : '').'</div>
                    <div class="row">
                    	'.$address.'
	                    <div class="col-md-9 col-sm-9">
	                    	'.$tavel_mode_form.$directions_result.'
	                    </div>
                    </div>
                </div><!--end-of-row--> 
            </div>
        </div>';
	}

	
	public static function posts( $atts = null, $content = null ) {
		// Prepare error var
		$error = null;
		// Parse attributes
		$atts = shortcode_atts( array(
				'template'            => 'templates/default-loop.php',
				'id'                  => false,
				'posts_per_page'      => get_option( 'posts_per_page' ),
				'post_type'           => 'post',
				'taxonomy'            => 'category',
				'tax_term'            => false,
				'tax_operator'        => 'IN',
				'author'              => '',
				'tag'                 => '',
				'meta_key'            => '',
				'offset'              => 0,
				'order'               => 'DESC',
				'orderby'             => 'date',
				'post_parent'         => false,
				'post_status'         => 'publish',
				'ignore_sticky_posts' => 'no',
				'autoplay' => 'no',
				'control' => 'yes',
				'column'			  => 4,
			), $atts, 'posts' );

		$original_atts = $atts;

		$author = sanitize_text_field( $atts['author'] );
		$id = $atts['id']; // Sanitized later as an array of integers
		$ignore_sticky_posts = ( bool ) ( $atts['ignore_sticky_posts'] === 'yes' ) ? true : false;
		$meta_key = sanitize_text_field( $atts['meta_key'] );
		$offset = intval( $atts['offset'] );
		$order = sanitize_key( $atts['order'] );
		$orderby = sanitize_key( $atts['orderby'] );
		$post_parent = $atts['post_parent'];
		$post_status = $atts['post_status'];
		$post_type = sanitize_text_field( $atts['post_type'] );
		$posts_per_page = intval( $atts['posts_per_page'] );
		$tag = sanitize_text_field( $atts['tag'] );
		$tax_operator = $atts['tax_operator'];
		$tax_term = sanitize_text_field( $atts['tax_term'] );
		$taxonomy = sanitize_key( $atts['taxonomy'] );
		// Set up initial query for post
		$args = array(
			'category_name'  => '',
			'order'          => $order,
			'orderby'        => $orderby,
			'post_type'      => explode( ',', $post_type ),
			'posts_per_page' => $posts_per_page,
			'tag'            => $tag
		);
		// Ignore Sticky Posts
		if ( $ignore_sticky_posts ) $args['ignore_sticky_posts'] = true;
		// Meta key (for ordering)
		if ( !empty( $meta_key ) ) $args['meta_key'] = $meta_key;
		// If Post IDs
		if ( $id ) {
			$posts_in = array_map( 'intval', explode( ',', $id ) );
			$args['post__in'] = $posts_in;
		}
		// Post Author
		if ( !empty( $author ) ) $args['author'] = $author;
		// Offset
		if ( !empty( $offset ) ) $args['offset'] = $offset;
		// Post Status
		$post_status = explode( ', ', $post_status );
		$validated = array();
		$available = array( 'publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash', 'any' );
		foreach ( $post_status as $unvalidated ) {
			if ( in_array( $unvalidated, $available ) ) $validated[] = $unvalidated;
		}
		if ( !empty( $validated ) ) $args['post_status'] = $validated;
		// If taxonomy attributes, create a taxonomy query
		if ( !empty( $taxonomy ) && !empty( $tax_term ) ) {
			// Term string to array
			$tax_term = explode( ',', $tax_term );
			// Validate operator
			if ( !in_array( $tax_operator, array( 'IN', 'NOT IN', 'AND' ) ) ) $tax_operator = 'IN';
			$tax_args = array( 'tax_query' => array( array(
						'taxonomy' => $taxonomy,
						'field' => ( is_numeric( $tax_term[0] ) ) ? 'id' : 'slug',
						'terms' => $tax_term,
						'operator' => $tax_operator ) ) );
			// Check for multiple taxonomy queries
			$count = 2;
			$more_tax_queries = false;
			while ( isset( $original_atts['taxonomy_' . $count] ) && !empty( $original_atts['taxonomy_' . $count] ) &&
				isset( $original_atts['tax_' . $count . '_term'] ) &&
				!empty( $original_atts['tax_' . $count . '_term'] ) ) {
				// Sanitize values
				$more_tax_queries = true;
				$taxonomy = sanitize_key( $original_atts['taxonomy_' . $count] );
				$terms = explode( ', ', sanitize_text_field( $original_atts['tax_' . $count . '_term'] ) );
				$tax_operator = isset( $original_atts['tax_' . $count . '_operator'] ) ? $original_atts[
				'tax_' . $count . '_operator'] : 'IN';
				$tax_operator = in_array( $tax_operator, array( 'IN', 'NOT IN', 'AND' ) ) ? $tax_operator : 'IN';
				$tax_args['tax_query'][] = array( 'taxonomy' => $taxonomy,
					'field' => 'slug',
					'terms' => $terms,
					'operator' => $tax_operator );
				$count++;
			}
			if ( $more_tax_queries ):
				$tax_relation = 'AND';
			if ( isset( $original_atts['tax_relation'] ) &&
				in_array( $original_atts['tax_relation'], array( 'AND', 'OR' ) )
			) $tax_relation = $original_atts['tax_relation'];
			$args['tax_query']['relation'] = $tax_relation;
			endif;
			$args = array_merge( $args, $tax_args );
		}

		// Fix for pagination
		if( is_front_page() ) { 
			$paged = ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1; 
		} else { 
			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1; 
		}
		$args['paged'] = $paged;

		// If post parent attribute, set up parent
		if ( $post_parent ) {
			if ( 'current' == $post_parent ) {
				global $post;
				$post_parent = $post->ID;
			}
			$args['post_parent'] = intval( $post_parent );
		}
		// Save original posts
		global $posts;
		$original_posts = $posts;


		// Query posts
		$posts = new WP_Query( $args );

		$posts->autoplay = $atts['autoplay'];
		$posts->column = $atts['column'];
		$posts->control = $atts['control'];	

		// Buffer output
		ob_start();
		// Search for template in stylesheet directory
		if ( file_exists( STYLESHEETPATH . '/' . $atts['template'] ) ) load_template( STYLESHEETPATH . '/' . $atts['template'], false );
		// Search for template in theme directory
		elseif ( file_exists( TEMPLATEPATH . '/' . $atts['template'] ) ) load_template( TEMPLATEPATH . '/' . $atts['template'], false );
		// Search for template in plugin directory
		elseif ( path_join( dirname( TP_PLUGIN_FILE ), $atts['template'] ) ) load_template( path_join( dirname( TP_PLUGIN_FILE ), $atts['template'] ), false );
		// Template not found
		else echo Tp_Tools::error( __FUNCTION__, __( 'template not found', 'perch' ) );
		$output = ob_get_contents();
		ob_end_clean();
		// Return original posts
		$posts = $original_posts;
		// Reset the query
		wp_reset_postdata();
		perch_query_asset( 'css', 'bohopeople-bootstrap' );
		perch_query_asset( 'css', 'bohopeople-default-blog' );
		if(($atts['template'] == 'templates/masonary-loop-sidebar.php') || ($atts['template'] == 'templates/masonary-loop.php')){
			perch_query_asset( 'js', 'bohopeople-masonry-scripts' );
		}
		return $output;
	}

	

}

new Tp_Shortcodes;



class Perch_Shortcodes_Shortcodes extends Tp_Shortcodes {
	function __construct() {
		parent::__construct();
	}
}

function home_slider( $atts = null, $content = null ) {
		$atts = shortcode_atts( array(
				'slides' => '',
				'button_text' => 'Join the Event',
				'button_link' => '',
				'countdown_time' => 'August 7, 2017 22:00:00',
				'slide_shape' => 'yes',
				'down_arrow' => 'yes',
			), $atts, 'home_slider' );

		do_action( 'perch/shortcode/home_slider', $atts );
		$output = '';
		$slides = (function_exists('vc_param_group_parse_atts'))? vc_param_group_parse_atts($atts['slides']) : array(
				array(
					'title' => __( 'FEEL THE ENERGY', 'electron' ),
					'value' => 'BREATHTAKING CROWD',
					'bg_image' => 'http://www.dezinethemes.com/envato/music/01/img/slider/slide1.jpg',
					'desc' => '',
				),
				array(
					'title' => __( 'Shake it out', 'electron' ),
					'subtitle' => 'Rock solid sounds',
					'desc' => '',
					'bg_image' => 'http://www.dezinethemes.com/envato/music/01/img/slider/slide2.jpg'
				),
				array(
					'title' => __( 'Electronic Madness', 'electron' ),
					'subtitle' => 'Thrilling performances',
					'desc' => '',
					'bg_image' => 'http://www.dezinethemes.com/envato/music/01/img/slider/slide3.jpg'
				),
				);


		$slidesID = (count($slides) > 1)? 'slides' : 'slide-single';
		$output .= '<div id="'.esc_attr($slidesID).'">';

		$output .= '<div class="slides-container">';
		$i = 1;
		foreach ($slides as $key => $value) {
			$output .= '<div class="slide'.(($i == 1)? ' active' : '').'">
                    <div class="img"><img src="'.esc_url($value['bg_image']).'" alt="'.esc_attr($value['title']).'" class="img-responsive"></div>
                    <div class="slide-caption">
                        <div class="container">
                            <div class="box">
                            '.(($value['title'] != '')? '<h1>'.esc_attr($value['title']).'</h1>' : '').'
                            '.(($value['subtitle'] != '')? '<span>'.esc_attr($value['subtitle']).'</span>' : '').'
                            '.(($value['desc'] != '')? '<br><span style="margin-top: 0">'.esc_attr($value['desc']).'</span>' : '').'
                            </div>
                        </div>
                    </div>
                </div><!-- end slide1 -->';
            $i++;    
		}
		$output .= '</div>';

		$output .= '<div class="slides-navigation">
                <a class="prev sqaureIconSec" href="#"> <i class="fa fa-chevron-left"></i></a>
                <a class="next sqaureIconSec" href="#"> <i class="fa fa-chevron-right"></i></a>
            </div><!-- end slides-navigation -->';

        $output .= ($atts['slide_shape'] == 'yes')? '<div class="shapes absShape">
                <div class="shape1 absShape"></div>
                <div class="shape2 absShape"></div>
                <!-- <div class="gradient absShape"></div>-->
            </div>' : ''; 

        $output .= ($atts['down_arrow'] == 'yes')? '<div class="holder"><i class="fa fa-chevron-down moreArrow moving"></i></div>' : '';      

        $output .= '<div class="container-fluid absShape">
                '.(($atts['countdown_time'] != '')? '<div class="BGprime first col-md-6 col-sm-6 col-xs-12">
                    <div class="countdown styled" data-countdown-time="'.$atts['countdown_time'].'"></div>
                </div>' : '').'
                '.(($atts['button_text'] != '')? '<div class="BGsecondary col-md-6 col-sm-6 col-xs-12">
                    <a class="btn join smooth" href="'.$atts['button_link'].'" title="Join the event">'.esc_attr($atts['button_text']).'</a>
                </div>' : '').'
                
            </div>';

		$output .= '</div>';

		
		return $output;
	}

	add_shortcode( 'perch_home_slider', 'home_slider' );
